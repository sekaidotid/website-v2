---
title: "Re:ZERO Season 2 Di Undur Karena Dampak COVID-19"
date: "2020-03-10T18:00:00+07:00"
---

![](./rezero.webp)

[Situs web resmi](http://re-zero-anime.jp/news/#news-200309-tv-3?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link) untuk anime Re:Zero kara Hajimeru Isekai Seikatsu mengumumkan pada 9 Maret bahwa musim kedua, yang dijadwalkan akan dimulai pada bulan April, akan ditunda hingga Juli 2020 karena efek global dari virus corona (COVID-19) yang telah menunda produksi pada musim mendatang.

![](./rem.webp)

Posting di situs web resmi dari komite produksi menyatakan bahwa "jadwal telah sangat terpengaruh" untuk produksi Re: ZERO karena wabah coronavirus di seluruh dunia, yang telah sangat mempengaruhi produksi anime outsourcing di China, serta acara di Jepang, seperti Anime Jepang 2020, yang dibatalkan. Pos mengatakan mereka "berencana untuk menyiarkan mulai Juli" dan untuk memeriksa situs web resmi dan akun media sosial untuk informasi lebih lanjut ketika masuk. \[[Crunchyroll](https://www.crunchyroll.com/anime-news/2020/03/09-1/rezero-season-2-has-been-delayed-due-to-the-ongoing-coronavirus-crisis-to-july-2020?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

---
title: "Kawaii tapi menyebalkan: Himouto Umaru chan, anime yang bikin semua orang gregetan"
date: "2015-05-01T22:12:03.284Z"
description: "Kawaii tapi menyebalkan: Himouto Umaru chan, anime yang bikin semua orang gregetan"
---

![](./umarunn.webp)

Himouto berasal dari kata imouto yang arti nya adik perempuan. Kenapa kok di tambah huruf “H” di depan nya kalu kata imouto sudah cukup untuk mewakili kata “adik perempuan”? Ya, huruf “H” itu berasal dari Bahasa slang dalam masyarakat Jepang yang di sebut “Himono-Onna”, yang tertuju pada wanita yang nampak sempurna di tengah kehidupan masyrakat, tapi menjadi sangat pemalas dalam kehidupan pribadi nya. Ya, judul yang sempurna untuk menggambarkan seorang Umaru Doma, si karakter utama dalam anime ini.

![Umaru yang terkenal “sempurna”](./Umaru-Doma.webp)
Umaru yang terkenal “sempurna”

Umaru ini, kalau lagi di sekolah tuh, bawaan nya begitu lembut dan “kawaii”, dia juga selalu menjadi juara kelas di kelas nya. Cantik, karismatik, dan serba bisa, adalah pembawaan dari seorang Umaru Doma saat dia berada di sekolah nya. Di lapangan, dia juga sangat ahli melakukan berbagai jenis olahraga. Pokok nya hampir mendekati sebagai sosok perempuan yang sempurna, deh. Tetapi saat ada di rumah, dia menjadi perempuan yang sangat pemalas, manja, suka minum cola, makan junk food, main game, pokok nya kesan nya “gak karuan”.

![Taihei, Kakak dari Umaru](./Taihei-Doma.webp)
Taihei, Kakak dari Umaru

Umaru memiliki Kakak Laki-laki yang untung nya sangat sabar dalam menghadapi sikap Umaru yang sangat menyebalkan sebagai Adik Perempuan, bernama Taihei Doma. Kepada Taihei lah, Umaru bermanja ria dan minta segala yang di inginkan nya. Taihei adalah sosok Kakak yang tidak bisa berlama-lama marah ke Umaru, tak peduli seberapa menyebalkan pun Adik nya (so sweet banget kan, kadang min Naomi pingin punya Kakak sabar semacam Taihei, hehe).

![Umaru si adik “sableng”](./Umaru-si-adik-sableng.webp)
Umaru si adik “sableng”

Cara Umaru untuk membuat Kakak nya memenuhi semua keinginan nya, ada dua cara. Ketika Umaru berada di luar, dia akan pura-pura menangis dan merasa menjadi perempuan yang tersakiti di samping Taihei. Otomatis kan, orang yang sekitar nya akan membenci Taihei, karena menganggap Taihei sebagai cowok yang gak baik karena tega membuat perempuan secantik Umaru menangis. Pada akhir nya Taihei pun akan memenuhi keinginan Umaru karena tidak enak dengan orang-orang sekitar yang melihat nya. Lain lagi ketika Umaru berada di dalam rumah, cara nya lebih frontal lagi. Dia akan menangis, teriak, dan melempari Taihei dengan barang-barang yang ada di sekitar nya. Pokok nya frontal luar biasa lah. Dan dia tidak akan berhenti sampai Taihei menyerah dan mengabulkan keinginan nya.

Oh iya, Umaru ini sudah ada season kedua nya juga lho. Meski di Season 2 nya, anime nya di kemas dengan alur cerita yang lebih serius, tapi tetap terasa kocak nya. Tapi, untuk kalian yang belum melihat season pertama nya, disarankan untuk nonton season pertama nya dulu agar tidak bingung dan kaget dengan sosok Umaru. See ya in next post ^-^

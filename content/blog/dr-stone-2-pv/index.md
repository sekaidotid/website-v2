---
title: "Anime Dr. Stone Season 2 Rilis Video Teaser"
date: "2020-07-04T04:00:00+07:00"
---

![Anime Dr. Stone Season 2 Rilis Video Teaser](./sekaidotid-drstone2.webp)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/pNKxaV3BNvs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

website resmi Anime Dr. Stone merilis video teaser pada hari Jumat (3 Juli 2020).

Anime Dr Stone season 2 sendiri direncanakan rilis pada musim dingin (Winter) 2021

Season baru ini akan menceritakan arc “Stone War”.

\[[Anime News Network](https://www.animenewsnetwork.com/news/2020-07-02/dr-stone-anime-2nd-season-teaser-previews-stone-wars-arc/.161371?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

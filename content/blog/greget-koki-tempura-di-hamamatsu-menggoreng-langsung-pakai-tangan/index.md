---
title: "Greget! Koki Tempura Di Hamamatsu Menggoreng Langsung Pakai Tangan!"
date: "2018-08-19T19:38:53+07:00"
description: "Greget! Koki Tempura Di Hamamatsu Menggoreng Langsung Pakai Tangan!"
---

![](./menggoreng-tempura-pakai-tangan.webp)

saat kita menggoreng tentu saja memakai alat bantu untuk membalik makanan seperti sudip. tetapi apa jadinya jika saat menggoreng menggunakan tangan langsung? tentu saja melepuh bukan.

hal ini tidak terjadi pada chef di restoran Tenkin yang berlokasi di Stasiun Hamamatsu, Prefektur Shizuoka.

hal tersebut dibagikan oleh pengguna Twitter @Yuku1991

> 現時点の人生で一番好きな飲食店は、浜松駅前にある天錦。税込 1,000 円で最高の天丼が食べられる。店主は職人技を極めすぎて天ぷら油に素手突っ込んで卵黄揚げてる。これ口で言うとみんなに「は？」って顔されるけど言葉通りのパフォーマンスだから動画で見て。 [pic.twitter.com/Ycu20RBECe](https://t.co/Ycu20RBECe)
>
> — ゆく nʞnʎ (@Yuku1991) [August 15, 2018](https://twitter.com/Yuku1991/status/1029722646047412229?ref_src=twsrc%5Etfw)

dalam video tersebut terlihat sang koki memegang wadah yang berisi kuning telur, lalu memasukkan kuning telur ke adonan tempura, kemudian memasukkan adonan ke wajan yang berisi minyak panas beserta jari nya

> ライデンフロスト現象（熱した鉄板の上の水滴が，鉄板に接した部分が水蒸気になることによって鉄板と直接触れずに水滴のまま長持ちするのと同じ現象）で，油の熱は手に直接伝わってはいないと思います．  
> ただ常人に出来る技でないのは間違いありません
>
> — 鯛焼きうどん (@taiyakiudon) [August 16, 2018](https://twitter.com/taiyakiudon/status/1030078468690571264?ref_src=twsrc%5Etfw)

Pengguna Twitter @taiyakiudon percaya ini adalah contoh dari efek Leidenfrost

Ketika cairan mendekati substansi lain yang jauh lebih panas daripada titik didih cair, dihasilkan uap yang menyebabkan efek isolasi.

Karena tangan koki dilapisi dengan adonan yang relatif dingin, kulitnya tidak rusak karena waktu yang singkat dalam panci minyak mendidih.

> 天丼は海老 2 尾、ハゼ(魚)、海苔、大葉の梅干し挟み、卵黄の天ぷらが乗っかってて、これの他にあさりの味噌汁、お漬物、そしてタイミングを見計らって出てくる海老の海苔巻きの天ぷら。これで 1,000 円。コスパお化け。近場行ったらみんなぜひ寄って。 [pic.twitter.com/ezyPJp8uLC](https://t.co/ezyPJp8uLC)
>
> — ゆく nʞnʎ (@Yuku1991) [August 15, 2018](https://twitter.com/Yuku1991/status/1029725328724254720?ref_src=twsrc%5Etfw)

pengguna Twitter @Yuku1991 menambahkan untuk harganya “hanya” 1000 Yen.

kalau di hitung secara kurs sih memang agak mahal 😀 terutama bagi mahasiswa seperti saya 😀

apakah anda berminat?

**Informasi Restoran**  
Tenkin / 天錦  
Alamat: Shizuoka-ken, Hamamatsu-shi, Naka-ku, Tamachi 325-29  
静岡県浜松市中区田町 325-29  
Buka 11:00.-13:20, 17:00-18:30  
Hari Rabu Tutup

Sumber : [SoraNews24](https://soranews24.com/2018/08/19/master-tempura-chef-in-hamamatsu-uses-his-bare-hands-to-cook-with-boiling-oil%E3%80%90video%E3%80%91/)

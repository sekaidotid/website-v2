---
title: "Daftar Anime Serial, Film, Manga Yang Terdampak COVID-19"
date: "2020-04-21T17:23:00+07:00"
---

![Daftar Anime Serial, Film, Manga Yang Terdampak COVID-19](./sekaidotid-rezero2.webp)

Berikut daftar Anime serial televisi, film & manga yang terdampak akibat COVID-19

## Serial Televisi

- Boruto: Naruto Next Generations
- Digimon Adventure:
- Healin' Good Precure
- One Piece
- Pocket Monsters
- Bungo and Alchemist -Gears of Judgement-
- Toaru Kagaku no Railgun T (juga menunda episode sebelumnya, tetapi dilanjutkan pada 28 Februari 2020 sebelum penundaan saat ini)
- Shokugeki no Souma: Gou no Sara (Season 5)
- Appare-Ranman!
- Cardfight!! Vanguard Gaiden if
- Fugou Keiji: Balance:Unlimited
- Houkago Teibou Nisshi
- IDOLiSH7 Second Beat!
- Sword Art Online: Alicization - War of Underworld 2nd Season
- No Guns Life second half
- Yahari Ore no Seishun Love Comedy wa Machigatteiru. Kan (Season 3)
- Anpanman (rekaman dialog terhenti)
- Sazae-san (rekaman dialog terhenti)
- A3! Season Autumn & Winter
- A3! Season Spring & Summer
- number24 (dilanjutkan pada 8 April 2020)
- Re:Zero kara Hajimeru Isekai Seikatsu 2nd Season
- Tsukiuta. The ANIMATION 2
- The Misfit of Demon King Academy
- Infinite Dendrogram (dilanjutkan pada 27 Februari 2020)

## Film

- Evangelion: 3.0+1.0: Thrice Upon A Time
- Love Me, Love Me Not
- Words Bubble Up Like Soda Pop
- Given
- Happy-Go-Lucky Days
- Fate/stay night: Heaven's Feel III. spring song (delayed for second time)
- Butt Detective: Tentō Mushi Iseki no Nazo
- Fushigi Dagashiya Zenitendō: Tsuritai Yaki
- Recycle Zoo: Mamore! Mokuyōbi wa Shigen Gomi no Hi
- Violet Evergarden
- Detective Conan: The Scarlet Bullet
- Eiga Crayon Shin-chan Gekitotsu! Rakuga Kingdom to Hobo Yonin no Yūsha
- Princess Principal: Crown Handler movie 1
- Doraemon: Nobita no Shin Kyoryū
- Looking for Magical DoReMi
- Eiga Precure Miracle Leap: Minna to Fushigi na 1-nichi
- Eiga Doraemon: Nobita no Shin Kyoryū
- Shimajirō to Sora Tobu Fun

## Live-Action

- Whisper of the Heart
- Cowboy Bebop
- Tokyo Revengers
- Keep Your Hands Off Eizouken!
- Kamen Rider Den-O: Pretty Den-O Tōjō!
- Grand Blue Dreaming
- Sonic the Hedgehog (Japanese release)
- Ultraman Taiga: New Gene Climax

## Manga dan Majalah

- Hana to Yume
- Young Animal
- MOE
- Morning two
- Ane Friend
- Young Magazine the 3rd
- good! Afternoon
- Bessatsu Shōnen Magazine
- Monthly Young Magazine
- Hatsu Kiss
- comic tint
- Honey Milk
- Shōnen Magazine R
- Kinnikuman
- Weekly Shonen Jump manga volumes
- Shonen Jump+ manga volumes
- Jump Square manga volumes
- Weekly Shonen Jump
- The Case Study of Vanitas

\[[Anime News network](https://www.animenewsnetwork.com/news/2020-04-21/list-of-anime-and-manga-affected-by-covid-19/.158799?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

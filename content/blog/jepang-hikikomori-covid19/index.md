---
title: "Pemerintah Jepang menunjuk hikikomori sebagai penasehat strategi karantina sendiri COVID-19"
date: "2020-04-06T19:30:00+07:00"
---

![Pemerintah Jepang menunjuk hikikomori sebagai penasehat strategi karantina sendiri COVID-19](./sekaidotid-hikikomori.webp)

Setelah dibenci oleh masyarakat, sekarang hikikomori mungkin menyelamatkan masyarakat.

Hikikomori adalah orang Jepang yang tertutup, orang yang tidak meninggalkan rumah mereka dalam keadaan apa pun. Mereka menjalani hidup mereka di kamar mereka di komputer mereka, memesan semua kebutuhan mereka secara online, dan biasanya hidup dari orang tua mereka.

Meskipun bukan sepenuhnya fenomena Jepang, hikikomori cukup sering membuat berita di sini ketika gaya hidup mereka mengakibatkan berminggu-minggu mereka hidup dengan mayat mayat ibu mereka ... atau bertahun-tahun mereka hidup dengan mayat mayat ibu mereka.

Tetapi dedikasi ekstrim hikikomori harus tetap berada di dalam rumah dengan segala cara adalah apa yang dibutuhkan Jepang, dan seluruh dunia, saat ini dengan penyebaran virus corona. Itulah sebabnya pemerintah Jepang baru-baru ini mengumumkan bahwa mereka telah menunjuk seorang hikikomori sebagai kepala dewan darurat khusus tentang "strategi karantina sendiri."

Dakura Maki, hikikomori asli Tokyo berusia tiga puluh empat yang diangkat menjadi anggota dewan, telah menghabiskan enam belas tahun terakhir hidupnya di dalam rumah orangtuanya di Nerima Ward.

Dia dipilih secara khusus untuk posisi ini setelah proses wawancara yang luas dengan lebih dari seribu calon hikikomori. Daftar ini berkurang setelah melihat hikikomori mana yang akan membalas e-mail, kemudian mengirim pesan teks, kemudian panggilan telepon terakhir. Dakura adalah satu-satunya kandidat yang menerima panggilan telepon dari organisasi pemerintah, meskipun ia dilaporkan hanya berbicara dengan napas basah dan berat.

Karena keterampilan sosialnya yang tinggi untuk seorang hikikomori, pengalamannya selama bertahun-tahun, dan situasi yang mendesak, ia segera diberi posisi.

Dibandingkan dengan bagian dunia lainnya, orang Jepang menghabiskan waktu lebih lama di luar rumah di tempat kerja mereka, di bar dengan rekan kerja, dan bahkan berpiknik melihat bunga. Syukurlah, Dakura dapat menyarankan beberapa strategi untuk membantu sesama warganya mengatasi dorongan sosial mereka.

Son Nawakenai, wakil ketua dewan, mengatakan ini tentang bekerja dengan hikikomori:

> “Usulannya untuk mengalihkan fokus dari produksi masker ke produksi dakimakura pada awalnya mengejutkan, tetapi masuk akal. Jika orang memiliki waifu dan suami 2-D untuk dipeluk, itu akan mencegah mereka dari kontak fisik dengan pasangan 3-D mereka. Heck, bahkan saya tidak sabar untuk pulang ke rumah untuk Miku-chan saya. Sekarang kalau saja hikikomori akan belajar bernapas melalui hidungnya ketika berbicara .... "

Salah satu saran Dakura yang saat ini sedang diberlakukan adalah pengembangan simulator kencan baru yang akan wajib bagi semua warga negara Jepang untuk mengunduh, bermain, dan menyelesaikannya sebelum diizinkan kembali ke luar. Petugas polisi akan diberi pengarahan tentang "rute gadis terbaik," dan mereka akan menginterogasi siapa pun yang tertangkap di luar untuk melihat apakah mereka menyelesaikan "Sparkling Venus" yang berakhir. Denda hingga 50.000 yen (US \$ 448) akan diberikan kepada mereka yang menolak untuk menepuk kepala Momoko-chan ketika dia mengakui cintanya kepada Anda.

Sejauh ini satu-satunya titik pertikaian antara Dakura dan dewan datang ketika ia ditanyai tentang apa yang harus dilakukan untuk sisi ekonomi karantina sendiri. Desakan Dakura bahwa setiap orang "tinggal meminta uang dari orang tua mereka" disambut dengan kebingungan.

Ketika dia diberitahu bahwa itu tidak layak untuk seluruh populasi, dia mulai terlihat berkeringat melalui kemeja kotak-kotak dan celana keringatnya, dan mulai memposting pesan kemarahan di papan pesan dan media sosial Jepang.

Menurut kalian bagaimana?

\[[SoraNews24](https://soranews24.com/2020/04/01/japanese-government-appoints-hikikomori-as-head-of-council-on-self-quarantine-strategies/?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

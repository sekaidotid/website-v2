---
title: "Kakushigoto Berkolaborasi Dengan Sayonara, Zetsubou-Sensei"
date: "2020-05-14T15:39:00+07:00"
---

![Kakushigoto Berkolaborasi Dengan Sayonara, Zetsubou-Sensei](./sekaidotid-cd.webp)

Adaptasi Anime TV dari manga Kakushigoto Kōji Kumeta mendapatkan CD kolaborasi dengan Sayonara, Zetsubou-Sensei, salah satu seri sebelumnya dari artis tersebut. Para pengisi suara Kakushigoto akan membawakan lagu-lagu bersama unit "Zetsubou Shoujo 2020", yang terdiri dari aktor suara dari kelas gadis 2-he. Sayonara, artis lagu tema Zetsubou-Sensei Kenji Ohtsuki juga akan berkontribusi pada CD.

Avex Pictures memposting sampel video Kamis lalu dari lagu "Ai ga yue yue" dan "Are kara".

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JlNHrRTkpYs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Pemeran lengkap dari aktor pengisi suara di bawah ini:

## Kakushigoto

- Rie Takahashi (Hime Gotō)
- Ari Ozawa (Silvia Kobu)
- Kaede Hondo (Hina Tōmi)
- Azumi Waki (Riko Kitsuchi)

## Sayonara, Zetsubou-Sensei

- Ai Nonaka (Kafuka Fūra)
- Marina Inoue (Chiri Kitsu)
- Yu Kobayashi (Kaere Kimura)
- Miyuki Sawashiro (Maria Tarō Sekiutsu)
- Ryoko Shintani (Nami Hitō)

CD ini dijadwalkan rilis pada 17 Juni.

Kumeta mengakhiri manga Sayonara, Zetsubou-Sensei pada Mei 2012. Sayonara, Zetsubou-Sensei menginspirasi tiga serial Anime TV dan beberapa OVA. Dia menerbitkan pengantar empat halaman untuk Kakushigoto di Kodansha's Monthly Shonen Magazine pada November 2015, dan kemudian menerbitkan bab penuh pertama di majalah yang sama pada Desember 2015. Serial ini telah menginspirasi adaptasi Anime televisi yang ditayangkan perdana pada 2 April.

\[[Anime News network](https://www.animenewsnetwork.com/interest/2020-05-12/kakushigoto-gets-collaboration-cd-with-sayonara-zetsubou-sensei/.159341?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

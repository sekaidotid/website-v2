---
title: "Fakta Menarik Wasabi Yang Jarang Diketahui Orang"
date: "2020-02-20T16:30:00+07:00"
---

![](./wasabi.webp)

Bagi yang hobi memakan sushi, pasti tidak asing dengan wasabi. Tumbuhan berwarna hijau ini memiliki rasa pedas yang unik, berbeda dengan sensasi yang kita rasakan ketika memakan cabai. Nah, di balik rasa uniknya, wasabi menyimpan beberapa fakta menarik. Penasaran? Ayo simak uraian 8 fakta menarik tentang wasabi berikut ini!

# Wasabi itu bukan lobak pedas

Banyak yang menyangka wasabi adalah horseradish atau lobak pedas, padahal bukan. Meskipun wasabi memang masih satu keluarga dengan lobak pedas, tetap saja tidak sama. Wasabi merupakan tanaman semacam rerumputan yang hanya ada di Jepang. Tanaman ini masih sekeluarga dengan tanaman persilangan lainnya, seperti mustard, kol, dan lobak.

# Wasabi ternyata sering “dipalsukan”

![](./wasabi-asli-palsu.webp)

Kamu tidak suka wasabi? Bisa jadi itu disebabkan karena wasabi yang masuk ke mulutmu bukanlah wasabi asli. Kebanyakan wasabi yang dijual di pasaran merupakan campuran dari lobak pedas, saus mustard pedas, dan pewarna serta perasa makanan. Campuran bahan-bahan ini akan menghasilkan rasa yang terlalu kuat, pahit dan sensasi terbakar. Padahal, wasabi asli memiliki rasa lebih halus yang bergantung pada proses penggilingan atau waktu pemrosesan dari dapur hingga dihidangkan di atas meja makan.

# Fungsi wasabi adalah untuk mencegah keracunan makanan

Banyak yang mengira adanya wasabi di atas sushi itu untuk menambah rasa pedas. Padahal sebenarnya, wasabi ada untuk mencegah keracunan dari ikan mentah. Hal ini disebabkan karena wasabi memiliki zat kimia bernama allyl isothiocyanate, zat kimia anti bakteri.

# Menanam wasabi itu sulit

![](./kebun-wasabi.webp)

Melihat banyaknya wasabi disajikan bersama makanan Jepang, mungkin kamu membayangkan kalau Jepang adalah negara yang penuh dengan wasabi. Padahal, nyatanya tidak begitu lho. Wasabi hanya bisa tumbuh jika memenuhi syarat tertentu. Pertama, wasabi hanya bisa tumbuh di lembah sungai di daerah pegunungan yang terlindungi dari sinar matahari dan akarnya harus masuk ke dalam air. Selain itu, tanaman ini hanya bisa tumbuh sekitar 1300 hingga 2500 meter di atas permukaan laut dan tidak akan tumbuh jika suhu air dibawah 8o C atau di atas 20oC. Wah, repot juga ya?

# Harga wasabi asli itu mahal

![](./harga-wasabi.webp)

Sebelumnya sudah dijelaskan kalau wasabi sering “dipalsukan”. Nah, harga mahal inilah yang jadi penyebabnya. Tapi melihat cara menanamnya yang sulit dan merepotkan, memang tidak aneh jika tanaman pedas ini dijual dengan harga tinggi. Meskipun begitu, wasabi tetap populer, bahkan banyak yang rela membayar mahal untuk wasabi asli.

# Rasa wasabi asli cepat hilang

Jika kamu mendapat kesempatan untuk mencicipi wasabi asli, sebaiknya kamu harus cepat-cepat memakannya. Alasannya, rasa wasabi asli yang diparut ternyata hanya bertahan selama 15 menit saja. Karena itu, jangan aneh jika melihat wasabi diparut secara dadakan di restoran Jepang.

# Wasabi asli itu awet

Bertentangan dengan rasa wasabi parut yang cepat hilang, wasabi asli (dalam bentuk utuh) dapat mempertahankan rasanya untuk beberapa bulan. Jadi, jika kamu menyimpan wasabi dalam jumlah banyak, tidak perlu khawatir.

# Wasabi bukan hanya untuk sushi

Di luar Jepang, wasabi biasanya disajikan bersama sushi atau sashimi. Namun di negara asalnya, kamu bisa menemukan wasabi disajikan bersama dengan makanan lain seperti soba atau saus di yakiniku, unagi chazuke, dan masih banyak lagi.

Sumber : \[[Japanese Station](https://japanesestation.com/8-fakta-menarik-wasabi-yang-jarang-diketahui-orang/?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

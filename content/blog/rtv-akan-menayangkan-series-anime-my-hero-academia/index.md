---
title: "RTV akan menayangkan series anime My Hero Academia"
date: "2020-07-02T10:00:00+07:00"
---

![My-Hero-Academia-RTV](./My-Hero-Academia-RTV.webp)

Konnichiwa, minna-san. Jumpa lagi bersama SEKAI.ID.

Kabar terbaru kali ini datang dari Rajawali Televisi ( RTV ) yang akan menayangkan series anime My Hero Academia.
Melansir dari postingan video dari akun sosial media RTV, terlihat cuplikan dari anime My Hero Academia dan di video tersebut bertuliskan SEGERA. Belum ada informasi lebih lanjut mengenai kapan mulai resmi tayang. Jadi, stay tune terus di SEKAI.id untuk informasi selanjutnya ya.

\[[Rajawali Televisi](https://web.facebook.com/langitRTV/videos/1585698984940524/)\]

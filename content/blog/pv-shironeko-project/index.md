---
title: "PV Terbaru Anime Shironeko Project Pamerkan Lagu Openingnya"
date: "2020-04-04T08:28:00+07:00"
---

![Shironeko Project](./sekaidotid-shironkeo-new-pv.webp)

Situs resmi dari “Shironeko Project: Zero Chronicle“, memulai sebuah streaming PV terbaru untuk adaptasi Anime-nya pada hari Jum’at kemarin.

PV ini juga diunggah dalam channel Youtube milik Colopl, berikut adalah cuplikannya:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YjCTEzc7-Pk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dalam PV berdurasi kurang lebih 1 menit 47 detik tersebut ditampilkan beberapa scene serta lagu opening yang dibawakan oleh Takanori Nishikawa dan ASCA dengan judul “Tenbin -LIbra-“. Sementara lagu ending yang belum ditampilkan akan dibawakan oleh Rei Yasuda dengan judul “through the dark“.

Bersamaan dengan PV tersebut, situs resminya juga menambahkan beberapa pengumuman seperti Anime ini akan mendapatkan total 12 episode. Sementara itu, edisi Blu-ray nya akan dibagi menjadi dua volume dan masing masing akan dikirimkan pada 26 Juni dan 29 Juli. Shironeko Project: Zero Chronicle direncanakan akan tayang pada 6 April jam 10 malam waktu Jepang.

![Shironeko Project](./sekaidotid-shironeko-new-pv-2.webp)

Seiyuu yang akan hadir beserta peran mereka adalah:

- Yuuki Kaji sebagai Prince of Darkness
- Yui Horie sebagai Iris
- Shōgo Batori sebagai Faios
- Chise Nakamura sebagai Sima
- Takuya Iwabata sebagai Teo
- Kensuke Matsui sebagai Alantia
- Katsuyuki Miura sebagai Valas
- Una Nagisa sebagai Groza
- Atsuyoshi Miyazaki sebagai Skears
- Kengo Takanashi sebagai Adelle
- Susumu Akagi sebagai King of Darkness
- Shozo Iizuka sebagai Bal

Anime akan menceritakan prolog “Zero Chronicle: Hajimari no Tsumi” yang hadir pada ulang tahun ketiga gamenya.

Masato Jinbo (Fate/kaleid liner Prisma Illya 2wei!, Chaos;Child) menulis skrip serta menyutradarai Anime di Project No.9 (Ryuo no Oshigoto!), Dan Yousuke Okuda (Sword Art Online the Movie: Ordinal Scale, Gochiusa) mendesain karakter. Taku Iwasaki (Bungo Stray Dogs) mengomposisi musik. Hiroki Asai (Colopl) menjadi produser eksekutif Anime, dan sutradara game Ryōji Tsunoda menjadi pengawas Anime

Shironeko Project diluncurkan pertama kali di Jepang pada tahun 2014 lalu dan diluncurkan dalam bahasa Inggris dengan judul Rune Story pada tahun 2015. Sementara itu Colopl juga mengumumkan bahwa game Shironeko Project yang baru akan dijadwalkan rilis untuk platform Nintendo Switch juga di tahun 2020.

\[[Jurnal Otaku](http://jurnalotaku.com/2020/04/04/pv-terbaru-anime-shironeko-project-pamerkan-lagu-openingnya/?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

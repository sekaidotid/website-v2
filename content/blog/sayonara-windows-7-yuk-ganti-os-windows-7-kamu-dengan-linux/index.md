---
title: "Sayonara Windows 7, Yuk Ganti OS Windows 7 Kamu Dengan Linux"
date: "2020-01-15T07:02:16+07:00"
---

![](./Windows_7_Madobe_Nanami_OS_tan_1920x1200.webp)

Microsoft secara resmi mengumumkan bahwa setelah 14 Januari 2020 tidak akan memberikan dukungan pada OS Windows 7

Yang berarti tidak ada lagi _security update_ di OS Windows 7

Tentu hal tersebut membuat PC / Laptop lebih beresiko terkena malware

Mau update ke Windows 10 tapi spek “kentang”, tenang kamu bisa memakai Linux

jika kamu baru mengetahui tentang Linux. Linux adalah sistem operasi sumber terbuka, yang berarti siapapun diperbolehkan mengembangkan &amp; mendistribusikan varian Linux secara gratis

Varian Linux (biasa di sebut distro Linux) sangat banyak sekali dengan berbagai macam kegunaan

seperti Kali Linux sebuah distro Linux yang berfokus untuk penetrasi jaringan ( biasa disebut hacking )

ada juga beberapa distro Linux yang berfokus untuk server

untuk kamu yang baru ingin migrasi ke Linux, saya sarankan untuk memakai Linux Ubuntu karena basis pengguna yang banyak tentu tidak susah mencari solusi utk masalah yang kamu hadapi

jika PC / Laptop kamu cukup “kentang” maka kamu bisa memakai Linux dengan desktop environment LXDE seperti Lubuntu yaitu Ubuntu dengan desktop environtment LXDE

atau jika kamu susah move on dari Windows maka kamu bisa memakai Zorin OS, yaitu sebuah distro Linux dengan tampilan yang mirip Windows

---
title: "Mahouka Koukou no Rettousei Season 2 Tayang Pada Juli 2020"
date: "2020-03-22T17:01:00+07:00"
---

![](sekaidotid-mahouka.webp)

<iframe src="https://streamable.com/s/wv3ii/abodiu" frameborder="0" width="100%" height="100%" allowfullscreen style="width: 100%; height: 100%; position: absolute;"></iframe>

Serial TV PV dan visual kunci baru untuk musim kedua dari serial Anime “Mahouka Koukou no Rettousei” (The Irregular at Magic High School) telah dirilis. Ini akan memiliki siaran perdana Juli 2020.

# Staf

Director: Risako Yoshida
Music: Taku Iwasaki
Studio: 8-Bit

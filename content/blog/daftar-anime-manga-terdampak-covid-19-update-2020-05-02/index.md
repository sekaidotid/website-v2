---
title: "Daftar Anime Serial, Film, Manga Yang Terdampak COVID-19 Update 2 Mei 2020"
date: "2020-05-02T15:43:00+07:00"
---

![Daftar Anime Serial, Film, Manga Yang Terdampak COVID-19](./sekaidotid-toaru-kagaku-no-railgun.webp)

Berikut daftar Anime serial televisi, film & manga yang terdampak akibat COVID-19

## Serial Televisi Anime

- A Certain Scientific Railgun T (episode 14 ditunda)
- Yu-Gi-Oh! Sevens
- Gundam Build Divers Re:RISE Season 2
- Black Clover
- Girl Gaku ~Sei Girls Square Gakuin~
- Chibi Maruko-chan
- Kingdom season 3
- Mewkledreamy
- Duel Masters King
- Major 2nd season 2
- Kiratto Pri☆Chan
- Mashin Eiyūden Wataru Shichi Tamashii no Ryūjinmaru
- The Irregular at Magic High School season 2 (Raihōsha-hen)
- The Promised Neverland season 2
- The Millionaire Detective - Balance: UNLIMITED (dilanjutkan 16 Juli 2020)
- Boruto: Naruto Next Generations
- Digimon Adventure:
- Healin' Good Precure
- One Piece
- Pocket Monsters
- Bungo and Alchemist -Gears of Judgement-
- Food Wars! Shokugeki no Soma: The Fifth Plate
- Appare-Ranman!
- Cardfight!! Vanguard Gaiden if
- Diary of Our Days at the Breakwater
- IDOLiSH7 Second Beat!
- Sword Art Online: Alicization War of Underworld Part 2
- No Guns Life second half
- My Teen Romantic Comedy SNAFU Climax
- Anpanman (rekaman dialog dibatalkan)
- Sazae-san (rekaman dialog dibatalkan)
- A3! Season Autumn & Winter (ditunda hingga Oktober 2020)
- A3! Season Spring & Summer (dilanjutkan 6 April 2020)
- number24 (dilanjutkan 8 April)
- Re:ZERO -Starting Life in Another World- second season
- Tsukiuta. The ANIMATION 2
- The Misfit of Demon King Academy
- Infinite Dendrogram (dilanjutkan 27 Februari 2020)

## Film Anime

- A Whisker Away (Nakitai Watashi wa Neko o Kaburu) (Stream di Netflix 18 Juni)
- Monster Strike The Movie: Lucifer Zetsubō no Yoake
- Eiga Precure Miracle Leap: Minna to Fushigi na 1-nichi (ditunda sampai ada pemberitahuan selanjutnya)
- Evangelion: 3.0+1.0: Thrice Upon A Time
- Love Me, Love Me Not
- Words Bubble Up Like Soda Pop
- Given
- Happy-Go-Lucky Days
- Fate/stay night: Heaven's Feel III. spring song (ditunda sampai ada pemberitahuan selanjutnya)
- Butt Detective: Tentō Mushi Iseki no Nazo
- Fushigi Dagashiya Zenitendō: Tsuritai Yaki
- Recycle Zoo: Mamore! Mokuyōbi wa Shigen Gomi no Hi
- Violet Evergarden
- Detective Conan: The Scarlet Bullet
- Eiga Crayon Shin-chan Gekitotsu! Rakuga Kingdom to Hobo Yonin no Yūsha
- Princess Principal: Crown Handler movie 1
- Stand By Me Doraemon 2
- Eiga Doraemon: Nobita no Shin Kyoryū (ditunda ke 7 Agustus 2020)
- Looking for Magical DoReMi
- Shimajirō to Sora Tobu Fune

## Live-Action

- The Cornered Mouse Dreams of Cheese
- Tonkatsu DJ Agetarō
- Whisper of the Heart
- Cowboy Bebop
- Tokyo Revengers
- Keep Your Hands Off Eizouken!
- Kamen Rider Den-O: Pretty Den-O Tōjō!
- Grand Blue Dreaming
- Toei studio productions
- Sonic the Hedgehog (Japanese release)
- Ultraman Taiga: New Gene Climax

## Manga dan Majalah

- Masahiro Itosugi's new series
- Hana to Yume
- Young Animal
- MOE
- Morning two
- Ane Friend
- Young Magazine the 3rd
- good! Afternoon
- Bessatsu Shōnen Magazine
- Monthly Young Magazine
- Hatsu Kiss
- comic tint
- Honey Milk
- Shōnen Magazine R
- Kinnikuman
- Weekly Shonen Jump manga volumes
- Shonen Jump+ manga volumes
- Jump Square manga volumes
- Weekly Shonen Jump
- The Case Study of Vanitas

\[[Anime News network](https://www.animenewsnetwork.com/news/2020-04-21/list-of-shows-films-manga-games-affected-by-covid-19/.158799?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

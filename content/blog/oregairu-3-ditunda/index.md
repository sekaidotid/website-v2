---
title: "Penayangan Anime Yahari Ore no Seishun Love Comedy wa Machigatteiru. Kan (Oregairu Season 3) Ditunda Akibat COVID-19"
date: "2020-04-07T21:58:00+07:00"
---

![Oregairu Season 3](./oregairu-season-3.webp)

Akun Twitter resmi untuk Anime Yahari Ore no Seishun Love Comedy wa Machigatteiru mengumumkan musim ketiga dari Anime, ditunda karena penyakit virus corona (COVID-19).

> この度、 今春から放送予定の『やはり俺の青春ラブコメはまちがっている。完』ですが
> 新型コロナウイルスの感染拡大の影響を受け、放送を延期させて頂くことになりました。
> 何卒、ご理解の程よろしくお願い申し上げます
>
> Twitter @anime_oregairu 5:00 PM Apr 7, 2020

Musim dijadwalkan untuk tayang perdana di TBS pada 9 April larut malam pukul 1:58 pagi (efektif 10 April). Alih-alih musim baru, Anime Yahari Ore no Seishun Love Comedy wa Machigatteiru musim kedua akan mulai ditayangkan kembali pada waktu itu. Anime tersebut juga akan mulai streaming di Amazon Prime Video di Jepang pada 9 April. Pertunjukan ini akan mencakup akhir cerita.

Sentai Filmworks telah melisensi seri ini untuk dirilis di luar Asia. HIDIVE ditetapkan untuk mulai streaming seri pada 10 April. Sentai Filmworks juga berencana untuk merilis Anime di video rumahan.

Para pemeran yang kembali meliputi:

- Takuya Eguchi sebagai Hachiman Hikigaya
- Saori Hayami sebagai Yukino Yukinoshita
- Nao Tōyama sebagai Yui Yuigahama
- Ayane Sakura sebagai Iroha Isshiki

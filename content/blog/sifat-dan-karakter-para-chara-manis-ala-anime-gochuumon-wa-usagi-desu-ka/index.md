---
title: "Sifat dan Karakter Para Chara Manis Ala Anime Gochuumon wa Usagi Desu Ka?"
date: "2018-08-14T16:10:27+07:00"
description: "Sifat dan Karakter Para Chara Manis Ala Anime Gochuumon wa Usagi Desu Ka?"
---

Halo Sobat Sekai.id, pada artikel kali ini, mimin Naomi ingin membahas soal karakter dan sifat dari para chara yang ada di anime “Gochuumon wa Usagi Desu Ka?”

![](./Gochuumon-wa-usagi-desu-ka.webp)

# Cocoa Hoto

![](./cocoa.webp)

Dia adalah karakter utama dari anime ini. Memiliki rambut coklat muda dan bermata ungu. Cocoa ini, merupakan gadis yang sangat ceria dan sedikit ceroboh. Dia mempunyai obsesi yang berlebihan untuk menjadi kakak yang baik bagaikan Kakak nya. Padahal dia ini adalah anak bungsu. Cocoa juga punya kemampuan menghitung yang luar biasa cepat, walaupun Cocoa sendiri tidak menyadari bahwa menghitung cepat adalah salah satu kemampuan nya yang sangat hebat.

# Chino Kafuu

![](./chino.webp)

Biasa di panggil Chino. Chino ini merupakan chara favorit mimin di anime ini (iyee, tau kalo kalian kagak nanya. Kan cuma mau beritau :v) Chino ini memiliki rambut panjang berwarna putih bagaikan salju dilengkapi dengan sepasang mata berwarna biru yang lembut. Penampilan nya ini sudah membuat mimin klepek-klepek sama Chino pada pandangan pertama xD Ditambah lagi suara nya yang sedikit tegas tetapi lembut. Chino ini lebih muda daripada Cocoa, tetapi lebih dewasa daripada Cocoa. Sejenis loli yang lembut tetapi tegas. Kadang juga ekspresi nya itu kawaii banget, terlalu manis sampai ke hati. xD

# Rize Tedeza

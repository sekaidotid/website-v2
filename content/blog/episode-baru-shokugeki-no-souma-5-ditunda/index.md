---
title: "Penayangan Episode Baru Shokugeki no Souma: Gou no Sara (Season 5) Ditunda Akibat COVID-19"
date: "2020-04-18T21:42:00+07:00"
---

![Penayangan Episode Baru Shokugeki no Souma: Gou no Sara (Season 5) Ditunda Akibat COVID-19](./sekaidotid-sns5.webp)

website resmi Anime Shokugeki no Souma pada tanggal 10 Maret 2020 mengumumkan bahwa episode 3 dan seterusnya ditunda karena dampak COVID-19

> 「食戟のソーマ 豪ノ皿」放送スケジュールの変更について
>
> いつも TV アニメ「食戟のソーマ」シリーズを応援していただき、誠にありがとうございます。
> 2020 年 4 月よりテレビ放送、配信を開始いたしました「食戟のソーマ 豪ノ皿」につきまして、新型コロナウィルス（COVID-19）の影響を受け、第 3 話以降の放送を当面の間、延期させていただくことにいたしました。
>
> なお、TOKYO MX および BS11 でのテレビ放送延期に伴う放送内容の変更について、下記のとおりお知らせいたします。
> 4 月 24 日（金） 「食戟のソーマ 神ノ皿」 第 1 話
> 5 月 1 日（金） 「食戟のソーマ 神ノ皿」 第 2 話
> 5 月 8 日（金） 「食戟のソーマ 神ノ皿」 第 3 話
> 今後のテレビ放送、配信に関しましては、決まり次第、公式ホームページおよび公式 twitter にてお知らせいたします。
> ご理解の程、何卒よろしくお願いいたします。
>
> [Shokugeki no Souma Website](http://shokugekinosoma.com/5thplate/news/index00090000.html?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)

untuk mengisi slot tayang di Tokyo MX dan BS11 maka sebagai gantinya di tayangkan Shokugeki no Souma: Shin no Sara (Season 4)

Perdana Menteri Jepang Shinzo Abe mengumumkan pada hari Kamis bahwa pemerintah nasional sedang memperluas keadaan darurat untuk penyakit coronavirus baru (COVID-19) secara nasional hingga 6 Mei. Abe sudah menyatakan keadaan darurat untuk tujuh prefektur pada 7 April, setelah membahas tentang masalah dengan panel penasihat ahli.

\[[Anime News Network](https://www.animenewsnetwork.com/news/2020-04-17/food-wars-shokugeki-no-soma-anime-delays-new-episodes-due-to-covid-19/.158711?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

---
title: "Bola Api yang Melintasi Langit Tokyo Terkonfirmasi Sebagai Meteor Yang Menghantam Prefektur Chiba"
date: "2020-07-14T16:42:00+07:00"
---

![Bola Api yang Melintasi Langit Tokyo Terkonfirmasi Sebagai Meteor Yang Menghantam Prefektur Chiba](./sekaidotid-tiamat.webp)

Sebuah bola api yang terlihat melintasi langit Tokyo pada awal bulan ini telah diidentifikasi sebagai sebuah meteor setelah pecahannya ditemukan di prefektur tetangga Tokyo. Hal tersebut diungkapkan oleh perwakilan museum sains nasional Jepang pada Senin (13/7).

Menurut Museum Nasional Alam dan Sains, meteor yang mendarat di sebuah kondominium di Narashino, Prefektur Chiba ini dipercaya memiliki diameter asal cukup besar meski kini terbagi dua. Kedua bongkahan meteor tersebut memiliki berat masing-masing 63 gram dan 70 gram.

Pecahan meteor tersebut pertama ditemukan oleh seorang penghuni wanita di kondominium yang terbangun akibat sebuah dentuman keras pada 2 Juli lalu, saat meteor tersebut jatuh. Ia menemukan pecahan batu tersebut di luar kamarnya saat paginya. Wanita ini juga menyimpan bongkahan tersebut setelah mengetahui berita tentang bola api tersebut, berpikir kalau bongkahan tersebut merupakan meteor.

![Footage meteor yang jatuh dan tertangkap kamera Hiratsuka City Museum](./sekaidotid-rekaman-meteor.webp)

Bekas benturan di pegangan pintu koridor membuat satu bongkah batu lagi ditemukan di lapangan kondominium 2 hari kemudian. Logam yang terdapat di bongkahan batu kedua juga berkarat karena terkena hujan, mengubah warnanya menjadi coklat.

Kini, museum yang tengah menganalisa bongkahan meteor tersebut mendeteksi adanya isotop aluminium, natrium dan mangan yang dihasilkan dari benturan sinar kosmik. Berkat beberapa isotop yang memang hanya bisa hidup selama beberapa hari saja, batu itu berhasil dikonfirmasi sebagai bagian dari meteor yang baru saja jatuh pada 2 Juli lalu.

Jatuhnya meteor ini menjadi momen jatuhnya meteor setelah meteor terakhir yang menghantam Jepang jatuh di sebuah property pribadi di Komaki, Prefektur Aichi pada 2 tahun lalu. Museum berencana untuk menemakan meteor tersebut "Narashino" dan mendaftarkannya ke Meteoritical Society. Jika diterima, meteor ini akan menjadi meteor ke-53 yang terdaftar di Jepang.

Sebelumnya diketahui jika meteor tersebut terlihat bergerak dari arah barat daya ke timur laut di langit ibu kota Jepang, memancarkan kobaran cahaya yang sebanding dengan cerahnya bulan purnama di langit malam.

Insiden yang terjadi pada pukul 2:30 pagi waktu Jepang ini langsung viral di sosial media. Netizen mengatakan bahwa mereka mendengar suara dentuman keras, sebagian dari mereka malah menyangka bahwa itu adalah suara dari tetangga mereka yang tinggal di lantai atas.

\[[Japanese Station](https://japanesestation.com/news/buzz-from-japan/bola-api-yang-melintasi-langit-tokyo-terkonfirmasi-sebagai-meteor?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

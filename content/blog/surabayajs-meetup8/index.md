---
title: "SurabayaJS Meetup #8"
date: "2020-05-13T02:30:00+07:00"
---

![SurabayaJS Meetup #8](./sekaidotid-sjs-meetup8.webp)

Hi hi kawan semua...

SurabayaJS akan mengadakan Meetup #8 yang mana akan dilaksanakan secara online melalui Zoom, yuk kita habiskan waktu ngabuburIT kita dengan hal yang bermanfaat dengan belajar bareng.

Meetup kali ini akan fokus pada pembahasan asynchronous pada JavaScript, bagaimana menangani SEO di JavaScript, serta bagaimana mengkonsumsi API milik Airtable menggunakan ExpressJS.

Materi akan disampaikan oleh :

1. Excel Daris Fadhillah (Software Developer)
   @exceldaris
   Materi : Introduce Asynchronous in JavaScript

2. Burhanuddin Ahmad (Software Developer)
   @burhannahm
   Materi: JavaScript and SEO, So What?

3. Miftahul Huda (Software Developer)
   @iniakunhuda
   Materi: Consume API from Airtable with ExpressJS

Informasi lengkap :
Hari dan Tanggal : Sabtu, 16 Mei 2020
Pukul : 15.00 WIB - Selesai

Online di Zoom !!

## [Register sekarang, sebelum kehabisan. Gratis!](https://s.sekai.id/surabayajs-meetup-8)

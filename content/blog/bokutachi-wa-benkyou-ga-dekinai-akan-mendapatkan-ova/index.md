---
title: "Bokutachi wa Benkyou ga Dekinai Akan Mendapatkan OVA"
date: "2019-05-14T12:48:58+07:00"
---

![](./Bokutachi-wa-Benkyou-ga-Dekinai.webp)

Pada terbitan ke-24 majalah milik Shueisha “**Shounen Jump**”, mengungkapkan bahwa _manga_ **“Bokutachi wa Benkyou ga Dekinai”** akan mendapatkan OVA. Rencananya perilisan OVA ini akan dibundel dengan volume ke-14 _manga_ buatan **Taishi Tsutsui** yang akan dirilis 1 November 2019. Pemesanan dini dapat dilakukan hingga 26 Agustus mendatang.

OVA ini akan mengadaptasi chapter 48 dan 49 _manga_-nya. Dimana pada _chapter_ ini berpusat pada Yuiga dan Asumi berkencan palsu di pantai dan kemudian Yuiga bertemu dengan gurunya, Mafuyu Kirisu yang bersembunyi karena kehilangan bikini bagian atasnya. Dikonfirmasi OVA ini akan berdurasi 25 menit. Tidak hanya itu, untuk edisi terbatas akan mendapatkan kartu ilustrasi “pengalaman berharga” dari Tsutsui sendiri.

_Manga_ ini berpusat pada Yuiga, seorang siswa yang mengejar beasiswa dikarenakan ia dari keluarga kurang mampu. Sekolahnya memberikan sebuah kondisi dimana Yuiga dapat mendapatkan beasiswa jika ia menjadi tutor untuk dua teman sekelasnya yang cantik untuk membantu mereka memilih sekolah yang mereka inginkan. Ogata tampak seperti siswi sains yang jenius dan Furuhashi adalah ahli literatur – akan tetapi Ogata ternyata membidik sekolah seni dan Furuhashi membidik sekolah sains. Dan keduanya kurang mengerti tentang materi diluar keahlian mereka.

Adaptasi _anime_ dari _manga_ ini tengah mengudara sejak 6 April lalu, Aniplex of America telah melisensikan anime ini dengan judul We Never Learn: BOKUBEN, dan anime ini tersedia secara streaming di Hulu, Crunchyroll, dan FunimationNow.

Studio Silver dan Arvo Animation bekerja sama untuk memproduksi animasinya. Serial TV _anime_ ini akan berlangsung selama 13 episode. Adaptasi *anime*nya disutradarai oleh Yoshiaki Iwasaki . Go Zappa mendapat peran untuk mengatur komposisi serinya dan desain karakternya dibuat oleh Masakatsu Sasaki.

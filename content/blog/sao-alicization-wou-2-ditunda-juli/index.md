---
title: "Penayangan Sword Art Online: Alicization - War of Underworld Season 2 Ditunda Juli 2020"
date: "2020-04-10T16:42:00+07:00"
---

![Penayangan Sword Art Online: Alicization - War of Underworld Season 2 Ditunda Juli 2020](./sekaidotid-sao-alicization-wou.webp)

Situs web resmi untuk Sword Art Online: Anime Alicization mengumumkan pada hari Jumat bahwa Sword Art Online: Anime Perang War of Underworld Bagian 2 ditunda hingga Juli karena efek dari penyebaran penyakit virus coronavirus COVID-19 di Jepang.

> 「SAO アリシゼーション WoU」最終章(2nd クール)放送・配信延期について
>
> 平素より「ソードアート・オンライン アリシゼーション War of Underworld」を応援していただき、誠にありがとうございます。
>
> 2020 年 4 月よりテレビ放送・配信予定の「ソードアート・オンライン アリシゼーション War of Underworld」最終章(2nd クール)につきまして、新型コロナウイルス「COVID-19」の感染拡大の影響を受け、放送及び配信を延期させて頂くことになりました。
>
> 現在 2020 年 7 月からの放送・配信を予定しておりますので、詳細な日時が決まりましたら、改めてアニメ公式サイトにて発表いたします。
> 楽しみにしていただいております皆様には、ご迷惑、ご心配をおかけすることとなり誠に申し訳ございません。
>
> なお、放送日程延期に伴い、４月より以下の放送を行う予定です。
>
> ※2020 年 4 月 11 日（土）放送予定の『ソードアート・オンライン』刊行 10 周年記念特番「作曲家 梶浦由記と SAO の音楽」は予定通り放送いたします。
>
> ※2020 年 4 月 18 日（土）放送予定の特番(総集編)は、本編放送と併せまして 2020 年 7 月に延期いたします。
>
> ※2020 年 4 月 18 日（土）からは昨年 2019 年 10 月より放送されておりました「ソードアート・オンライン アリシゼーション War of Underworld」1st クール（全 12 話）を再放送いたします。
>
> ご理解の程、何卒よろしくお願いいたします。
>
> 2020 年 4 月 10 日
> SAO-A Project
>
> <https://sao-alicization.net/news/?article_id=53963>

dari cuplikan diatas, Anime Sword Art Online: Alicization - War of Underworld Season 2 yang dijadwalkan akan disiarkan pada hari Sabtu, 18 April 2020 akan ditunda hingga Juli 2020.

Sword Art Online: Alicization War of Underworld Season 2 adalah Sword Art Online: "Final Season," Anime Alicization dijadwalkan untuk tayang perdana di Jepang pada 25 April. Anime itu dijadwalkan untuk streaming di Crunchyroll, HIDIVE, Funimation, dan Hulu .

\[[Anime News Network](https://www.animenewsnetwork.com/news/2020-04-10/sword-art-online-alicization-war-of-underworld-anime-2nd-part-delayed-to-july/.158461?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

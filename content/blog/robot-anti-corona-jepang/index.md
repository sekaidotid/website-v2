---
title: "Jepang Bikin Ugo Robot Untuk Perangi Corona"
date: "2020-07-14T21:00:00+07:00"
---

![Ugo-Robot-Buatan-Jepang-Perangi-Corona](./robot-ugo.webp)
<small>Ugo Robot Buatan Jepang Perangi Corona (Foto: Reuters/Kim Kyung-Hoon)</small>

Mira Robotics startup asal Jepang, mengembangkan sebuah robot yang dinamai Ugo untuk memperkuat tenaga kerja di Jepang yang semakin menipis.
Namun ketika ancaman virus Corona melanda mereka menawarkan mesinnya tersebut sebagai alat untuk memerangi wabah COVID-19.

"Virus Corona telah menciptakan kebutuhan akan robot karena mereka dapat mengurangi kontak langsung antara manusia," ujar Kent Matsui CEO Mira Robotics yang dilansir dari Reuters.

Fitur terbaru dari robot ini dapat dikendalikan dari jarak jauh serta kemampuan tangannya yang menggunakan sinar ultraviolet untuk membunuh virus pada gagang pintu.

Penurunan populasi yang belum pernah terjadi sebelumnya yang menyebabkan penyusutan jumlah tenaga kerja lebih dari setengah juta orang per-tahun serta minat yang enggan untuk membawa tenaga kerja asing memicu pengembangan robot di Jepang. Meningkatnya virus Corona dapat memajukan pekerjaan pengembangan robot.

Mira Robotics Ugo memiliki sepasang lengan robot yang dapat disesuaikan tingginya yang terpasang di atas roda. Ia bisa dioperasikan dari jarak jauh melalui koneksi nirkabel (wireless) dengan laptop dan pengontrol game.

Laser pengukur jarak yang dipasang di pangkalan membantu navigasi. Sementara panel di bagian atas menampilkan mata untuk memberikan penampilan yang lebih ramah.

![Ugo-Robot-2](./robot-ugo-2.webp)

Dikatakan Matsui dibutuhkan waktu sekitar 30 menit untuk mempelajari cara menggunakan robot ugo dengan setiap operator yang mampu mengendalikan sebanyak empat mesin.

Robot Ugo bisa disewa sekitar harga USD 1.000 atau sekitar Rp 14 jutaan perbulannya. Dengan robot ini bisa digunakan sebagai penjaga keamanan, melakukan inspeksi peralatan dan membersihkan toilet dan area lain di gedung perkantoran.

Startup Matsui yang baru berusia dua tahun sejauh ini hanya memiliki satu Ugo yang beroperasi di gedung perkantoran di Tokyo.

\[[Detik.com](https://inet.detik.com/cyberlife/d-5047784/jepang-bikin-ugo-robot-untuk-perangi-corona)\]
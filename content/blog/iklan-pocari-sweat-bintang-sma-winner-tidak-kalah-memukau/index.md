---
title: "Iklan Pocari Sweat Bintang SMA Winner Tidak Kalah Memukau"
date: "2020-02-25T11:00:00+07:00"
---

![](./bintang-sma-winner.webp)

Seru dan membanggakan bagi kita fans animanga Indonesia jika ada sesuatu dari negara kita yang muncul dalam sebuah animanga, apalagi jika visualnya terlihat indah dan kece. Dan itulah yang muncul dalam iklan Pocari Sweat Indonesia yang sempat viral beberapa bulan lalu. Nah menariknya lagi, ternyata iklan itu ada sequel! Yaitu iklan Anime Pocari BintangSMA Winner, yang tak kalah indah dengan yang pertamanya 😉

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/DCfk7tc_KqE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Sebelumnya pada bulan September 2019 kemarin Pocari Sweat Indonesia merilis iklan berbentuk anime yang menampilkan dua remaja, Ayu yang tinggal di Bali, dan Reza yang tinggal di Jakarta. Ceritanya adalah mengenai Ayu yang ingin ke Jakarta untuk mengikuti campaign Bintang SMA Pocari Sweat, ajang cari bakat online terbesar di Indonesia yang diadakan Pocari Sweat Indonesia.

Iklan tersebut jadi viral karena keindahannya yang dibandingkan dengan anime Shinkai Makoto. Menunjukkan setting pulau Bali dan kota Jakarta yang indah dan kontras antara satu sama lain, seperti kontras tempat tinggal kota Taki dan desa Yuzuha di anime Shinkai Kimi no Na wa.

Beberapa minggu setelah itu akhirnya diperlihatkan kenapa ada kemiripan tersebut, dan itu karena memang animator dan sutradara iklan tersebut juga terlibat dalam produksi dan animasi anime Kimi no Na wa, yaitu Pak Shinomiya Yoshitoshi.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fBKxZh5fejI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Dan kini Pak Shinomiya kembali lagi terlibat dengan Pocari Sweat dan Indonesia, dengan sekali lagi terlibat dalam pembuatan iklan kedua kompetisi Bintang SMA! Dan yang kali ini visualnya tak hanya masih indah warna-warni, tapi juga berupa gabungan animasi 2D dan tampilan 3D nyata.

Iklan baru itu mengumumkan pemenang dari kompetisi Bintang SMA Pocari Sweat Indonesia, dan ia adalah gadis muda bernama Nada Syakira Asiya! Dimana ia juga tampil langsung memainkan biola pada iklan tersebut, dan mengumumkan adanya kompetisi BintangSMA lagi nantinya.

Dan yang menarik lagi dari iklan kedua ini dibandingkan yang pertama, adalah dengan terang-terangan memperlihatkan list credit para staff yang memproduksi iklan tersebut bersama Shinomiya. Diantaranya adalah studio anime MAPPA yang juga memproduksi anime Kakegurui, Ushio & Tora, dan Dorohedoro pada musim ini.

![](./bintang-sma-winner-credit1.webp)

![](./bintang-sma-winner-credit2.webp)

Menariknya iklan ini juga bisa jadi pemanasan bagi fans band Sukima Switch karena band itu akan tampil di Indonesia pada bulan Maret 2020 nanti, dengan BGM iklan itu memakai aransemen biola dari lagu Zenryoku Shonen band tersebut, yang merupakan BGM iklan pertamanya. \[[Akiba Nation](https://www.akibanation.com/iklan-anime-pocari-bintangsma-winner-juga-indahhh/?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

---
title: "Spoiler Game Love Convention Cara Mendapatkan Good Ending Semua Karakter"
date: "2019-10-09T13:14:49+07:00"
---

![Cara Mendapatkan Good Ending Semua Karakter Love Convention](./Screenshot_2019-10-03-16-42-55-24.webp)

Hai kembali lagi bersama SEKAI.ID . kali ini SEKAI.ID mambahas “Spoiler Game Love Convention Cara Mendapatkan Good Ending Semua Karakter” kalian pasti sebel kan kalau dapat bad ending terus? ya sama 😀 yuk baca sampai habis

**Informasi Alergen : Postingan Ini 10 Milyar Persen Mengandung Spoiler. Bagi Yang Alergi Terhadap Spoiler Disarankan Untuk Tidak Membaca Postingan Ini**

# Informasi Dasar Game Love Convention

# Apa Itu Love Convention?

Love Convention adalah game bergenre dating simulation visual novel yang bercerita tentang seluk beluk dunia budaya pop Jepang

# Cara Bermain

Pemain dapat membaca cerita yang disajikan di game dan membuat pilihan berdasar cerita yang terjadi. Cerita berjalan dan berubah berdasarkan pilihan yang dibuat

# Karakter &amp; Akhir Cerita

Ada 5 karakter dan kurang lebih 13 ending yang bisa didapatkan player. Detail karakter dapat dibaca di bawah ini

![](./chara_1.webp)
![](./chara_2.webp)
![](./chara_3.webp)
![](./chara_4.webp)
![](./chara_5.webp)
![](./chara_6.webp)

# Sistem Game

Prolog tidak mempengaruhi jalan cerita. Jadi save di hari ke-6 (akhir prolog) biar lebih mudah mengulang rute-rutenya

Tahan tombol back dari smartphone android kalian untuk mempercepat teks cerita

Sistem si game ini adalah poin. Jadi setiap pilihan akan ada nilainya (mulai +3,+2,+1,0 hingga -3) ini yang digunakan untuk menentukan ending dan secret scene dari berbagai rute.

data diatas kami dapatkan dari Discord Server Love convention

kami tidak mengetahui detail poin utk setiap pilihan. utk RAIT Games &amp; Japan Culture Daisuki boleh dong kirim source code nya ke kami 😀 biar kami bisa mengintip poin2 setiap pilihan 😀

kami memainkan game Love Convention berulang kali sampai mendapatkan good ending

# Cara Mendapatkan Good Ending Enako Joestar

![](./Screenshot_2019-10-07-18-35-43-13.webp)

Cara Mendapatkan Good Ending Enako Joestar Game Love Convention

Enako Joestar memiliki 2 ending : good ending, bad ending

enaknya mengajak siapa ya? pilih **Enako**

“Terima kasih ya telah mengajakku hari ini” pilih **sama sama**

ingin bertanya apa ke Enako? pilih **tentang hobi**

apa selera musik Enako aneh? pilih **cocok kok**

Apa yang ingin kamu katakan ke Enako? pilih kamu **cakep**

enaknya ngapain sambil nunggu Enako? pilih **ikut handshake**

ingin ngomong apa ke Yuko? pilih **aku baru saja tahu kamu**

Enako ingin jalan jalan ke mall pilih **Enako suka jalan jalan ke mall?**

Apa kamu punya mimpi? pilih **punya lah**

Ingin bermain di mana? pilih **kolam ombak**

mungkin kah selama ini kehendak mu di kontrol orang lain? pilih **iya aku merasa begitu**

apa kamu ingin menunggu Enako? pilih **jalan jalan dulu**

Enako terlihat menikmati pertunjukan Kamen Writer barusan? pilih **ayo foto bareng Kamen Writer**

Enako terlihat terganggu saat mengobrol dengan seseorang? pilih **ajak foto dengan Kamen Writer**

soal popcorn dan minum, enaknya gimana? pilih **belikan minum saja**

ingin nonton apa? pilih **Assemble the Start Game**

sepertinya Enako ingin mengajakmu pergi? pilih **jujur**

Apa yang selama ini ingin kamu sampaikan ke Enako? pilih **mimpiku adalah bersama mu**

**(bad ending enako adalah di tikung sama machon)**

![](./Screenshot_2019-10-09-09-03-14-24.webp)

Cara Mendapatkan Good Ending Enako Joestar \*Game Love Convention

# Cara Mendapatkan Good Ending Monica Rahardja

![](./Screenshot_2019-10-11-10-07-19-45.webp)

Cara Mendapatkan Good Ending Monica Rahardja Game Love Convention

Monica Rahardja memiliki 3 ending : yaitu good ending, normal ending, bad ending

Apakah aku harus jadi pendukungnya? **Mendukung**

apa tanggapan mu terhadap komentar buruk ? **balas komentar buruk**

Menurutmu bagaimana menjadi orang menyenangkan? **aku tidak tahu**

apa yang sebaiknya kamu perbuat? **jaga machon**

kamu ingin naik ferris whel dengan siapa? **Momo (klo pilih Yuko menjadi rute Yuko)**

kamu ingin tanya apa ke momo? **kenapa suka cosplay**

bagaimana sebaiknya menanggapi tanggapan sinis Yuko? **Apa Maksudmu?**

sepertinya Momo butuh teman curhat? **ajak bertemu**

apakah aku harus harus mencoba memberikan saran? **beri solusi**

Ayah Momo terlihat marah. **ikut campur**

Bagaimana kamu harus memotivasi Momo?. **ingatkan momen bertemu**

**(jika kalian tidak mendapat respon, sudah pasti bad end. karena Momo bunuh diri)**

Ingin beli apa? **artbook**

apa yang kamu kagumi dari momo? **kamu cantik**

**(normal end adalah saat momo naik kepanggung bilang kalian semua pacarku :v )**

![](./Screenshot_2019-10-11-10-07-07-79.webp)

Cara Mendapatkan Good Ending Monica Rahardja Game Love Convention

# Cara Mendapatkan Good Ending Yuko G. Florencia

![](./Screenshot_2019-10-12-17-28-58-71.webp)

Cara Mendapatkan Good Ending Yuko G. Florencia

**utk rute Yuko pertama ambil rute Momo saat di ferris whel pilih Yuko kemudian yakinkan Yuko**

kenapa kamu ingin dekat dengan Momo? **nggak ada alasan khusus**

apa kekuatan terbesar Yuko? **keberanianmu**

bagaimana menurut mu tentang pendapat Yuko? **kamu benar**

barang apa yang cocok utk kado momo? **belikan barang pajangan**

apa menurutmu momo itu spesial? **oh iya dong**

kamu siapanya Yuko? **asisten**

suasana menjadi tidak terkendali. **minta mereka diam**

kamu bertemu Yuko. **aku harus ngapain?**

ada pengunjung cari gara2. **usir**

apakah dunia entertainment penuh kebohongan. **ya**

tanggapan tentang mimpi Yuko. **perjalanan mu panjang ya?**

tanggapan tentang keinginan Yuko. **apa kamu tidak bahagia?**

tanggapan tentang keluh kesah Yuko. **kejar yang kamu percayai**

yuko terlihat lelah. **ambilkan minuman**

bagaimana rasanya dipaksa menjadi asisten Yuko? **enggak sebel kok**

menurut mu Momo itu bagaimana. **Dia teman yang baik**

Yuko menghubungi mu. **aku senggang kok**

yuko tidak mau menemui momo. **enggak mau?**

yuko terlihat sebal. **aku cuma ingin tahu**

yuko merasa momo tidak menyempatkan diri lagi untuknya. **kamu harusnya mengerti**

yuko sepertinya terlihat gugup. **tanyakan kenapa gugup**

![](./Screenshot_2019-10-12-17-28-32-52.webp)

Cara Mendapatkan Good Ending Yuko G. Florencia

# Cara Mendapatkan Good Ending Viva Dona

![](./Screenshot_2019-10-24-11-35-00-14.webp)

Cara Mendapatkan Good Ending Viva Dona

Klub game?. **“Oke. aku bantuin”**

apa kamu akan terus terang ke Mcahon?. **Jujur**

Soal klub game, seharusnya bertanya pada siapa? **Tanya Machon**

Cari Jonathan di mana?. **Cari di rooftop**

Apa yang harus dikatakan pada Jonathan?. **Kami ingin membuat klub game**

sepertinya Jonathan tidak tertarik. **Kamu tidak tertarik untuk berkompetisi**

Apa yang pertama kali harus dilakukan oleh klub game? **Menentukan kegiatan untuk klub**

Apa yang pertama kali harus dilakukan oleh klub game?. **Terima Tawaran**

Apa yang akan kamu lakukan. **tidak ikut**

Apa yang akan kamu lakukan? **tetap tidak ikut**

Berdasarkan besar sudutnya, jenis segitiga ABC adalah. **Tumpul**

“compete” in this sentence closely associated to. **Fight**

Kamu bertemu Dona dan Jo. **“Bantu aku belajar dong”**

Kekeringan dapat berakibat turunnya … tanaman dan merugikan petani. **Produksi**

Mean data … adalah. **6,7**

Berdasarkan besar sudutnya, jenis segitiga ABC adalah. **Tumpul**

Kekeringan dapat berakibat turunnya … tanaman dan merugikan petani. **Produksi**

Inti kalimatnya adalah. **Doni mengoptimalkan kinerjanya**

“Compete” in this sentence closely associated to. **Fight**

I spend my time at the pool because … is my hobby. **Swimming**

apa yang ingin dilakukan setelah ujian. **Bersantai di rooftop**

Bagaimana ujiannya. **Lancar**

**(first bad end adalah saat gagal try out)**

Apa yang ingin kamu diskusikan dengan Dona?. **Tentang Kita berdua**

Aku juga menyukaianya

mereka berdua terlihat down. **evaluasi performa**

siapa yang harus di evaluasi. **Evaluasi Dona**

Apa yang harus dilakukan untuk menyemangati mereka?. **Beri ancaman**

Role apa yang seharusnya kamu pakai? **jadi tanker**

Ingin ganti role?. **tetap di role**

**(second bad end adalah saat di tinggal untuk membelikan mereka minuman Jonathan malah nembak dona)**

![](./Screenshot_2019-10-19-18-15-40-55.webp)
Cara Mendapatkan Good Ending Viva Dona

# Cara Mendapatkan Good Ending Tristania Arvaniti

![](./Screenshot_2019-10-25-09-36-55-67.webp)

Cara Mendapatkan Good Ending Tristania Arvaniti

Kenapa kamu di panggil ke ruang guru?. **“Tidak bu, saya tidak tahu”**

Bu Tania merasa tidak bersalah membuatmu menunggu lama. **Marahi Tania**

Bu Tania terlihat jengkel dengan pesan yang masuk. **Diam**

Apa itu pernikahan?. **“Sesuatu yang sakral”**

Kapan seharusnya orang menikah?. **“Ketika siap”**

Berapa harga satu keychain? **25.000**

Berapa harga satu poster dan satu keychain? **45.000**

Bu Tania terlihat kecewa dengan perlakuan panitia. **Hibur bu Tania**

Kata apa yang harus dicari? **Komik bu Tania**

Sebaiknya beli apa untuk bu Tania?. **Coklat**

Apa yang harus kamu lakukan? **Diam dan lanjutkan obrolan**

Apa yang harus kamu tahu isi komik dari “Forbidden Feeling”?. **“Tidak Tahu”**

Bagaimana menurutmu? Apakah Panitia salah? **“Tapi bu Tania juga salah”**

Menurut bu Tania, tidak ada yang melarang tema-tema seperti itu di event?. **“Benarkah?”**

Bu Tania terlihat kesal dengan pertanyaan ku. **“saya hanya bertanya”**

Bu Tania terlihat marah dengan pendapatku? **“saya minta maaf”**

Bu Tania mendapat telepon yang kelihatannya penting?. **Abaikan**

Ingin membelikan minuman apa untuk bu tania?. **Parfait**

Bagaimana menurutmu tentang keputusan bu Tania? **Pilihan tepat**

senang ya menikmati masa muda. **ibu menikmati masa muda tidak?**

apa yang kamu pikirkan tentang masa depan mu? **saya takut masa depan saya**

apa yang kamu pilih? **Tidak Keduanya**

Dona dan Jo terlihat dekat? **Mereka pacaran ya?**

Ketika dewasa, kamu akan dikelilingi berbagai tuntutan yang nggak kamu inginkan. **apa yang ibu inginkan**

sepertinya masalah bu Tania lebih dalam. **Tanyakan tentang kebahagiaan bu tania**

Apa menurutmu yang harus bu Tania lakukan? **ajak orangtua diskusi**

apa menurutmu orang tua bu Tania akan mendengarkan? **tetap ajak orangtua komunikasi**

komik bu Tania terbaru ternyata sukses. **selamat**

kapan kamu ingin menyatakan perasaan ke bu Tania? **nanti dulu**

bu Tania melihatmu kurang fokus. **nggak apa-apa**

kapan kamu ingin menyatakan perasaan ke bu Tania? **Langsung Confess**

akhirnya mendapatkan good ending <s>Kana Kojima</s> Tristania Arvaniti

![](./Screenshot_2019-10-25-09-36-21-86.webp)

Cara Mendapatkan Good Ending Tristania Arvaniti

# Ara Akazora Secret Chapter

![](./Screenshot_2019-10-24-17-24-46-43.webp)setelah mendapatkan semua good ending love convention semua karakter, kamu didatangi oleh Ara Akazora yang menanyaimu apakah kamu sudah bahagia / belum?

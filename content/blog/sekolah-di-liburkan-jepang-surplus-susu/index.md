---
title: "Sekolah Di Liburkan, Jepang Surplus Susu"
date: "2020-03-08T14:05:00+07:00"
---

![](./dekomori-susu.webp)

Pemerintah Jepang baru-baru ini umumkan bahwa kegiatan belajar mengajar dari sekolah dasar hingga menengah atas resmi diberhentikan terkait wabah COVID-19 yang kian menyebar di seluruh penjuru negara. Pemberhentian KBM ini tak hanya berdampak dari sisi pendidikan, namun juga ke industri peternakan. Bagaimana bisa?

Diwartakan oleh Sankei News, Kementerian Pertanian, Kehutanan, dan Perikanan melaporkan bahwa dari sekitar 20.000 ton susu yang didistribusikan setiap harinya, sekitar 2.000 ton-nya dikirimkan ke sekolah-sekolah. Susu ini dikhususkan untuk para siswa-siswi saat makan siang. Dengan berhentinya KBM di sekolah, otomatis terdapat surplus sekitar 30.000 ton susu dari produsen. Pemerintah sendiri menginginkan surplus ini untuk diolah menjadi bubuk susu, mentega dan produk susu lainnya. Akan tetapi tak semua pabrik pengolahan memiliki alat untuk memproduksi output tersebut.

Terkait dengan masalah ini, sejumlah pengguna Twitter memberikan respon tegas kepada pemerintahnya. Salah satunya adalah penasihat kooperatif pertanian dengan nama pengguna Miyabi (@miyabi02294470).

> メーカーは問いませんので
> 牛乳、国産の乳製品のご購入、消費をお願いします 🙇⤵
> 学校給食に使われる予定だった牛乳が余ってしまう可能性があります
> 牛さんのお乳は、出す量を加減できません
> 絞ったお乳が売れないと
> 酪農家さんが生活できなくなり
> 最悪廃業に追い込まれてしまいます
>
> Twitter @miyabi02294470, 5:03 PM · Feb 29, 2020

“Tak peduli merek apa itu, dimohon untuk membeli susu, susu lokal dan minumlah. Ada kemungkinan bahwa susu yang dialokasikan untuk makan siang di sekolah akan masih tersisa. Sapi perah tidak dapat mengatur jumlah susu yang mereka produksi. Jika susu yang diambil dari sapi-sapi ini tidak dapat dijual, peternak sapi perah tak dapat memenuhi kebutuhannya. Dalam skenario terburuk, mereka dapat bangkrut dari bisnisnya.” \[[Jurnal Otaku](http://jurnalotaku.com/2020/03/06/sekolah-sekolah-di-jepang-tutup-surplus-persediaan-susu-tak-bisa-terhindarkan/?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

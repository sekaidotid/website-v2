---
title: "[SPECIAL REVIEW] KISAH SANG ROBIN HOOD ALA JEPANG MASIH BERLANJUT  &#8211; PERSONA 5 THE ANIMATION DARK SUN"
date: "2019-01-04T11:47:23+07:00"
---

![](./Persona-5-The-Animation-Dark-Sun.mp4_003058560.webp)

Konnichiwa, minna-san. Berjumpa lagi dengan SEKAI.ID.Kini Bang Jagad punya segmen baru untuk review, ya Give Aplause to SPECIAL REVIEW  
Prok !!!Prok!!!!Prok!!!^w^

Sebelumnya sempat terpikirkan untuk Bang Jagad mau review ulang anime yang belum sempat terposting karena sempat hiatus untuk beberapa waktu. Memang tidak dapat dipungkiri bahwa penulisnya banyak yang liburan karena banyak kecewa dengan anime di beberapa musim sebelumnya yang temanya monoton kalo gak rpg ya isekai ya cinta tikung dan sebagainya. Kebetulan ini Bang Jagad mau sikat review untuk anime yang sudah terlewat beberapa bulan dengan langsung ke topiknya yaitu episode spesialnya. Langsung saja kita lihat reviewnya CHECK THIS OUT

# IMPRESSION – PERSONA 5 THE ANIMATION DARK SUN

![](./Persona-5-The-Animation-Dark-Sun.mp4_000578720.webp)

Well, cuma untuk kali ini yang tidak ada quote yang dapat mewakili episode ini karena quote sudah tampil pada ova The Day Breakers. Kenapa harus ada episode spesial? Tidak lain tidak bukan bertujuan untuk menjawab rasa penasaran dari penonton setia Persona 5 The Animation yang diakhiri dengan ending tragis yang very Cliffhanger.

![](./Persona-5-The-Animation-Dark-Sun.mp4_001031480.webp)

Cerita berlanjut setelah Amamiya Ren a.k.a Joker yang dibunuh oleh rekannya Akechi Goro a.k.a Crow seorang detektif pengguna Persona. Kematian tersebut ternyata ada kematian palsu yang direncanakan Joker dan anggota Phantom lainnya untuk mengelabuhi Crow. Bak peribahasa diatas awan masih ada awan, Joker ingin mengejar penjahat yang sesungguhnya yang berkomplotan dengan Crow yaitu Shido Masayoshi,seorang pemimpin partai serikat masa depan yang menjadi dalang dari semua kejahatan pada Persona 5.

Bang Jagad kali ini tidak memberi Spoiler dalam episode berdurasi kurang lebih 50 menit ini agar minna-san tetap terus penasaran sebelum melihat anime ini karena setelah menonton ini cerita masih akan berlanjut di Star and Ours pada Maret 2019.  
 Sekian dan terima kasih dan terus pantau terus Bang Jagad dimana lagi kalau bukan web kesayangan kalian semua SEKAI.ID.

Mata Ashita

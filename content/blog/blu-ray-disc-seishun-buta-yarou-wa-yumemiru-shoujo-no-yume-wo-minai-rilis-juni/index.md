---
title: "Blu-ray Disc Seishun Buta Yarou wa Yumemiru Shoujo no Yume wo Minai Rilis Pada Bulan Juli 2020"
date: "2020-04-11T17:59:00+07:00"
---

![Blu-ray Disc Seishun Buta Yarou wa Yumemiru Shoujo no Yume wo Minai Rilis Pada Bulan Juli 2020](./sekaidotid-seishun-movie.webp)

Aniplex of America mengumumkan pada hari Jumat bahwa mereka akan merilis Seishun Buta Yaro wa Yume-Miru Shōjo no Yume wo Minai Blu- ray Disc di Amerika Utara pada 30 Juni. Perusahaan mulai menayangkan iklan untuk pengumuman tersebut.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GBr4m5muwp8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Rilis ini akan menampilkan kemasan dengan seni eksklusif dan bundel booklet khusus dengan panduan referensi karakter, bahan desain adegan, dan seni storyboard.

Aniplex of America dan Funimation Films memutar Anime di Amerika Serikat pada 2 dan 3 Oktober, dan di Kanada pada tanggal 4 dan 5. Aniplex of America memutar film pemutaran perdana Amerika Serikat di Anime Expo pada 7 Juli.

Film ini dibuka di Jepang pada 15 Juni, dan debut di # 7 di akhir pekan pembukaannya. Film ini memperoleh total kumulatif 373.520.090 yen (sekitar US \$ 3,43 juta).

Aniplex of America menjelaskan kisah film ini:

> Di Fujiwara, di mana langit cerah dan lautan berkilau, Sakuta Azusagawa berada di tahun kedua sekolah menengahnya. Hari-hari kebahagiaannya dengan pacar dan kakak kelasnya, Mai Sakurajima, terganggu dengan penampilan naksir pertamanya, Shoko Makinohara. Untuk alasan yang tidak diketahui, ia bertemu dua Shokos: satu di sekolah menengah dan yang lainnya telah menjadi dewasa.
> Ketika Sakuta mendapati dirinya hidup tak berdaya bersama Shoko, Shoko yang dewasa membawanya ke dekat hidung, menyebabkan keretakan besar dalam hubungannya dengan Mai.
> Di tengah-tengah semua ini, ia menemukan bahwa sekolah menengah Shoko menderita penyakit serius dan bekas lukanya mulai berdenyut ...

Film ini menampilkan staf dan pemeran yang kembali, termasuk sutradara Sōichi Masui, penulis naskah Masahiro Yokotani, perancang karakter Satomi Tamura, rencana penangkap rubah komposer musik, dan studio produksi animasi CloverWorks.

Film ini mengadaptasi volume novel ringan keenam dan ketujuh, Seishun Buta Yaro wa Yume-Miru Shōjo no Yume wo Minai dan Seishun Buta Yarō wa Hatsukoi Shōjo no Yume wo Minai

\[[Anime News Network](https://www.animenewsnetwork.com/news/2020-04-10/aniplex-usa-to-release-rascal-does-not-dream-of-a-dreaming-girl-film-on-bd-in-june/.158472?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

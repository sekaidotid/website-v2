---
title: "Penayangan Mahouka Koukou no Rettousei: Raihousha-hen (Season 2) Ditunda Akibat COVID-19"
date: "2020-04-26T04:23:00+07:00"
---

![Penayangan Mahouka Koukou no Rettousei: Raihousha-hen (Season 2) Ditunda Akibat COVID-19](./sekaidotid-mahouka.webp)

dari situs resmi Anime Mahouka Koukou no Rettousei mengumumkan bahwa Anime Mahouka Koukou no Rettousei: Raihousha-hen (season 2) di tunda ke bulan oktober 2020

> 「魔法科高校の劣等生 来訪者編」放送・配信延期について
> 平素より「魔法科高校の劣等生 来訪者編」を応援いただき、誠にありがとうございます。
>
> 2020 年 7 月より放送・配信予定の「魔法科高校の劣等生 来訪者編」につきまして、新型コロナウイルス感染症「COVID-19」の感染拡大の影響を受け、放送・配信を延期させていただくことになりました。
>
> 現在 2020 年 10 月からの放送・配信を予定しておりますので、詳細な日時が決まりましたら、改めてアニメ公式サイトにて発表いたします。
>
> 楽しみにしていただいております皆様には、ご迷惑、ご心配をおかけすることとなり誠に申し訳ございません。ご理解の程、何卒よろしくお願いいたします。
>
> 2020 年 4 月 24 日
> 魔法科高校 2 製作委員会

Anime ini mengadaptasi volume 9-11 dari light novel

\[[Anime News network](https://www.animenewsnetwork.com/news/2020-04-24/the-irregular-at-magic-high-school-anime-season-2-delayed-to-october-due-to-covid-19/.158934?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

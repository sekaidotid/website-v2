---
title: "Review &#8211; Idol yang berjuang bangkit dari kematian – ZOMBIELAND SAGA"
date: "2018-12-23T08:53:13+07:00"
---

Konnichiwa, minna-san. Kita jumpa lagi nih di SEKAI.ID di segmen review anime. Berikan tepuk tangan  
Prok!prok!prok :B

Sudah sekian lama kita tidak bertemu,kita sekarang bertemu di musim bagusnya anime. Pasti sudah kangen ya? Sabar aja boss karena yang saya bahas lagi freshnya dan lagi hits banget. Kita sekarang mau bahas anime musik bergenre idol.WOOO^o^OOO!!!!!. Ya baginya yang wibu atau ngidol siapkan lightstick kalian dan siapkan mental,kenapa? Karena idol yang ini beda dari biasanya karena yang jadi idol para zombie.What? ZOMBIE? Ya silahkan disimak berikut ini

# FIRST IMPRESSION – Zombieland Saga

![](./neko-streaming.webp)

Nice Quote:

Meski kami sudah mati, kami ingin mewujudkan mimpi kami. Tidak, meski kami sudah mati, kami akan mewujudkan mimpi kami.

Jujur aja, sebelum adanya opening kesan pertamanya dari judul agak membuat banyak orang berfantasi dan berekspektasi.Why ZOMBIELAND SAGA? Kesan yang saya tangkap saat itu mungkin wah, ini ada horror ini atau mungin death game kayak DEADMAN WONDERLAND atau mungkin kayak game online yang ada di fb (N\*nja Saga). Yang berpikir seperti saya mungkin terlalu banyak nghayal hahaha ^o^. Okay, okay abang akan jelasin.

![](./yurination.webp)

Cerita berawal dari seorang gadis yang sangat kagum terhadap idol bernama Minamoto Sakura. Suatu hari ketika berangkat sekolah dia mengalami sebuah kecelakaan sehingga membuatnya tewas. 10 tahun kemudian dia terbangun di sebuah mansion tua namun belum sadar bahwa dia sudah mati. Ketika dikejar gerombolan zombie wanita dia keluar dari mansion lalu tertembak polisi karena melihat Sakura sebagai zombie. Ketika terbangun lagi dia bertemu dengan seseorang yang narsis bernama Tatsumi Kotaro. Kotaro ingin menjadikan Sakura sebagai idol bersama zombie wanita lainnya untuk mempopulerkan prefektur Saga.

# WHY IS MAPPA?

![](./480p_ZS_03_Anitoki.mp4_001182556.webp)

Anime ini diproduksi oleh Cygames x Studio Mappa tentu ini jadi hal biasa bagi Cygames untuk produksi bertema idol untuk kepentingan gamenya namun bagaimana dengan studio MAPPA? Ya, awalnya saya juga heran produksi sebelumnya tidak pernah bertemakan Cute Girls Doing Cute Thing karena mereka lebih bertemakan fantasi seperti seperti Shingeki no Bahamut tapi jangan khawatir anime ini menampilkan grafis 2d dan 3d yang sangat memuaskan dibanding anime MAPPA sebelumnya.

# FROM ROCK TO THE RAP GOD

![](./youtube.webp)

Ini sebenarnya bukan kali pertama ada genre anime musik yang menampilkan genre musik yang beraneka ragam,ex: Show By Rock. Namun menariknya disini satu grup idol menampilkan berbagai macam genre. Pada anime ini selain idol harus menyanyikan lagu yang cute ada juga heavy metal, rap battle, sampai ada genre kayak k-pop gaya gang motor gitu. Salut dah ini mah namanya totalitas kerja.

![](./hot963.webp)

So, sekian dari review hari ini. Kita jumpa pada review berikutnya. THRILLER

---
title: "[REVIEW] KISAH ROMEO &amp; JULIET YANG MENGUNDANG TAWA – KISHUKU GAKKOU NO JULIET"
date: "2018-12-28T08:19:49+07:00"
---

![](./Kishuku-Gakkou-no-Juliet-2.webp)

Konnichiwa, minna-san. Jumpa lagi bersama SEKAI.ID di segmen REVIEW ANIME. KEEP SMILE  
 ^o^ ^-^ ^O^

Di musim ini kita kedatangan anime hits yang bergenre romance comedy. Wooo^O^ooo akhirnya datang juga setelah penantian panjang. Mungkin kalau kita ngomong romance jarang sekali ada pure romance comedy. Di musim kemarin banyak sekali romancenya mengutamakan kalangan dewasa sampai lupa terhadap cinta sejati sehingga yang ada banyak saling tikung menikung sampai lagu cangkul cangkul indonesia jadi tikung tikung tersebar di media youtube. Itu sebenarnya bukan salah mereka membuat anime tersebut untuk kepentingan komersial, namun kepentingan akan seni dari drama percintaan tersebut jadi berkurang karena harus membuat plot yang bergenre netorare. Kalau yang memang suka dengan romance yang benar benar sejati dan setia hanya dengan satu pasangan wajib menyimak review ini. Hehe?Mau tahu?Mau tahu  
Tenang BANG JAGAD akan gorengkan fresh from the pan!Haha^o^!  
Okay, check this out!

FIRST IMPRESSION – KISHUKU GAKKOU NO JULIET

![](./Kishuku-Gakkou-no-Juliet-7.webp)

Mungkin kalau kalian lihat pembukaan opening ini ekspektasi saya ini anime bakal terjadi peperangan. Pertama ada dua kubu yang satu hitam yang satu putih lalu bawahnya ada ubin kotak warnanya ya hitam putih^o^. Kalau tidak mungkin, mereka saling tempur ada dua guild disisi kiri dan kanan terus kalau pembukaan ada suara dari langit berkata M\*BILE LEGEND, DOUBLE KILL, TRIPLE KILL,dsb.Okay, abang stop curhatnya mari kita review.

![](./Kishuku-Gakkou-no-Juliet.webp)

Nice Quote:  
 Kalau aku bisa bersamamu , maka dunia akan kuubah

Kisah ini bersetting di zaman modern di Dahlia Gakuen dimana terdapat dua kubu yang saling bermasalah yaitu Black Doggy kubu yang khas dengan tingkah barbar dan White Cats kubu yang sangat elit. Pemimpin Black Doggy Inuzuka Romio sejujurnya memedam perasaan cinta kepada pemimpin White Cats Juliet Persia sejak kecil. Suatu ketika, anggota dari Black Doggy berusaha menyergap Persia. Inuzuka Romio bermaksud menyelamatkan Persia dari anggota Black Doggy tersebut, namun disalahmengertikan oleh Persia. Malam hari, Inuzuka Romio mendapat surat tantangan dari Persia dan mendatanginya. Disana secara spontan Inuzuka Romio memberitahukan perasaannya kepada Persia ketika menerima tantangan beradu pedang. Pada akhirnya Persia menerimanya dan mereka mulai menjadi pacar rahasia.

WATCH OUT IT’S A BOMB

![](./Kishuku-Gakkou-no-Juliet-5.webp)

Karena anime ini temanya adalah berpacaran secara rahasia maka tidak tanggung-tanggung mereka harus menjaga identitas mereka agar tidak terbongkar oleh kedua kubu di setiap episode dan mereka harus bertemu secara sembunyi mulai dari ketemu malam hari, ketemu di danau, sampai seperti yang ada pada gambar. Sungguh lol anime ini

TUKANG EMBER COMING

![](./Kishuku-Gakkou-no-Juliet-4.webp)

![](./Kishuku-Gakkou-no-Juliet-6.webp)

Tidak semua pacaran kucing-kucingan ini berjalan mulus. Inilah sisi positif dari konflik anime ini. Kalau ketahuan yang tertimpa sial juga dari prianya seperti harus jadi pembantu, tunggangan, sampai kamar asrama pun dijajah. Ngakak deh pasti terhibur

TRIVIA

![](./Kishuku-Gakkou-no-Juliet-8.webp)

Sekedar info aja kalau anime dari adaptasi manga berjudul sama karangan Kaneda Yousuke mendapat banyak respon positif dari banyak mangaka seperti mangaka dari fairy tail. Jadi anime ini bisa jadi jaminan 100% puas dan menghibur deh.

Sekian untuk review anime kali ini. Tetap terus pantau SEKAI.ID karena BANG JAGAD akan terus update untuk mereview. Matta ashita

---
title: "Kantor Pemerintahan di Jepang Akan Pekerjakan Orang yang Menganggur Akibat Corona"
date: "2020-04-21T07:26:00+07:00"
---

![Kantor Pemerintahan di Jepang Akan Pekerjakan Orang yang Menganggur Akibat Corona](./sekaidotid-hikikomori.webp)

Pemerintah Jepang punya cara sendiri untuk menjaga warganya agar tetap memiliki penghasilan di tengah pandemi ini.

Melihat banyaknya orang yang kehilangan pekerjaan, kini kantor-kantor pemerintahan di level prefektur dan kota mulai merekrut orang-orang yang kehilangan pekerjaan tadi.

Dilansir Japan Times, program ini utamanya ditujukan kepada orang-orang yang dipecat atau yang tawaran kerjanya dibatalkan.

Program ini punya dua misi khusus. Pertama, untuk membantu para pengangguran yang kehilangan pekerjaannya akibat Covid-19. Dan yang kedua, untuk tetap mengamankan banyak tenaga kerja yang dinilai punya kemampuan khusus dalam penanggulangan Covid-19.

Pemerintah Kobe pada bulan Maret lalu juga berjanji akan mempekerjakan 100 orang tua tunggal yang kehilangan pekerjaan akibat wabah ini.

Selain itu, 100 pelajar yang rencananya akan lulus bulan ini dan tawaran pekerjaannya dibatalkan juga akan dipekerjakan.

Hingga Rabu pekan lalu, baru satu orang yang sudah mulai bekerja di bawah program ini.

Pihak departemen personalia dari kantor pemerintahan Kota Kobe juga meyakinkan kalau mereka akan siap menerima pendaftar kerja kapan saja.

Selain Kobe, pemerintah Osaka juga nampaknya akan menerapkan program serupa. Walikota Osaka, Ichiro Matsui kabarnya akan mulai merekrut 50 orang dengan kontrak kerja selama satu tahun.

\[[Kompasiana](https://www.kompasiana.com/prihastomowahyuwidodo/5e94a366d541df459c7a56e5/pekerja-di-jepang?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

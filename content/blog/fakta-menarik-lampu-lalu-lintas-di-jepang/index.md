---
title: "Fakta Menarik Lampu Lalu Lintas Di Jepang"
date: "2020-03-08T14:00:00+07:00"
---

![Fakta Menarik Lampu Lalu Lintas Di Jepang](./lampu-lalin-jepang-1.webp)

## mempunyai warna merah kuning biru

ya di jepang warna untuk melaju adalah biru bukan hijau

bahasa Jepang hanya memiliki istilah untuk 4 warna. Yaitu warna hitam, putih, merah, dan biru (ao).

Setelah beberapa lama menggunakan 4 istilah warna ini, lalu di masa milenial pertama, muncullah istilah midori yang dipakai untuk menyebutkan warna hijau. Namun makna midori bagi bangsa Jepang ini masih sulit diingat dan selalu dibayang-bayangi warna biru (ao). Mereka masih sering tertukar bahasanya untuk menyebutkan hijau (midori) menjadi biru (ao).

![Fakta Menarik Lampu Lalu Lintas Di Jepang](./lampu-lalin-jepang-2.webp)

saat ini supaya sesuai ketentuan internasional (hijau) & tetap dibenarkan dalam penyebutan "ao" (biru) maka pemerintah jepang mengambil jalan tengah, yaitu warna cyan (biru-hijau)

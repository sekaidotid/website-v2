---
title: "Fakta Menarik Dan Rangkuman Manga Citrus"
date: "2019-05-22T20:59:02+07:00"
---

![](./mei-yuzu-menikah.webp)

Hi kali ini SEKAI.ID membahas “Fakta Menarik Dan Rangkuman Manga Citrus” pada sudah tau kan kalau anime citrus rilis pada bulan januari 2018.  
berhubung sudah januari 2018 maka tinggal menunggu rilis, SEKAI.ID akan memberikan Rangkuman Manga Citrus karena pasti banyak yang ga sabaran pengen tau cerita nya tapi malas baca manga nya.  
iya kan? hehehe 😀

# Fakta Menarik Manga Citrus

1. Yuzuko Aihara &amp; Mei Aihara merupakan saudara sepupu
2. Amemiya-Sensei bertunangan dengan Mei Aihara dengan tujuan mendapatkan sekolah
3. Mei Aihara sudah mengetahui bahwa dirinya hanya “dimanfaatkan” oleh Amemiya-Sensei
4. Sebenarnya Mei Aihara merupakan pribadi yang ceria, ia berubah menjadi pendiam semenjak di tinggalkan ayah nya
5. Momokino Himeko (Wakil Ketua OSIS) juga mencintai Mei Aihara
6. Sebenarnya Shou Aihara (Ayah nya Mei Aihara) adalah orang yang baik. Ia meninggalkan Mei Aihara supaya Mei Aihara tidak mengikuti langkahnya yang gagal.
7. Misuzawa Matsuri (Teman Masa Kecil Yuzu Aihara) Cinta Kepada Yuzu Aihara
8. Misuzawa Matsuri menggangap Mei Aihara sebagai saingan beratnya untuk mendapatkan cinta nya Yuzu Aihara
9. Misuzawa Matsuri Berusaha Menjebak Mei Aihara
10. Sara Tachibana menyukai Mei Aihara
11. Karena Shou Aihara dan Ume Aihara menikah, maka Yuzu Aihara dan Mei Aihara menjadi saudara tiri
12. Yuzu Aihara berusaha keras kerja paruh waktu supaya dapat beli cincin pasangan
13. Siraho Suzuran menyukai Mei Aihara

**Postingan Belum Selesai. Harap Bersabar menunggu Update**

# Rangkuman Manga Citrus

## Vol.1 Chapter 1: love affair?!

Yuzu Aihara pindah ke sekolah khusus perempuan karena terbiasa berdandan maka pada hari pertama berurusan dengan OSIS karena ribut2 datanglah ketua OSIS yang bernama Mei Aihara.

![](./Yuzu-Aihara-Dan-Mei-Aihara-OSIS.webp)Mei Aihara tiba-tiba memeluk Yuzu Aihara dan dengan cepat nya mengambil smartphone milik Yuzu Aihara, tentu saja Yuzu Aihara langsung lemas dan menangis.

Setelah mengambil smartphone nya, Yuzu Aihara kembali ke kelas  
dan tiba2 di tengah perjalanan tanpa sengaja melihat Mei Aihara dan Amemiya-Sensei berciuman!

saat di rumah tiba2 datang Mei Aihara, tentu saja Yuzu Aihara kaget karena belum di beri tahu oleh ibu nya jika saudara perempuan nya adalah si Mei Aihara Tersebut

saat di kamar Yuzu Aihara membahas soal ciuman tadi dan tiba Mei Aihara langsung mencium saudara nya tersebut yaitu Yuzu Aihara,

setelah beberapa detik berciuman Mei Aihara hanya mengucapkan satu kalimat “Itu Hanya Ciuman” dan pergi.

## Vol.1 Chapter 2: one’s first love

Saat di mall Mei Aihara bercerita dia merasa kesepian karena ayah nya sudah tidak pulang selama 5 tahun (jangan2 bang toyib 😀 )

dan Yuzu Aihara Terbesit di pikiran nya “kalau kepala sekolah adalah kakek nya Mei Aihara berarti kakek saya juga dong”

Yuzu Aihara melamun sampai tidak sadar terjatuh ke air mancur.  
Mei Aihara sebenarnya sudah menarik Yuzu Aihara tetapi malah ikut tercebur juga.

sesampai nya di rumah mereka langsung mandi bersama, saat berendam di bath tub bersama mereka saling “menyentuh” dan berciuman (jadi gak sabar ingin lihat anime nya 😀 . **update : adegan di manga sama di anime agak berbeda ternyata** )

![](./Mei-Aihara-Yuzu-Aihara-Mandi-Bareng.webp)

Yuzu Aihara dengan pede nya menyapa kepala sekolah dengan sebutan “kakek”  
langsung saja kepala sekolah dengan tanpa ekspresi bilang “siapa kamu?” dan tidak di perbolehkan masuk wilayah sekolah.

tidak hilang akal Yuzu Aihara memanjat tembok sekolah dan akhirnya bisa masuk. tanpa sengaja mendengan pembicaraan telepon Amemiya-Sensei bahwa hubungan dengan Mei Aihara hanya untuk mendapat uang saja

Saat upacara Yuzu Aihara melakukan hal “bodoh” dengan mengumumkan hubungan Amemiya-Sensei dan Yuzu Aihara

saat kerumah Yuzu Aihara melihat ibu menangis dan berkata Mei-Chan dibawa pergi.

## Vol.1 Chapter 3: love my sister xxx

Ibu berkata Mei-Chan di bawa pergi oleh kakek dan Mei-Chan tidak menolak

keesokan hari nya di sekolah Mei Aihara hanya diam tanpa menjawab sekata pun dari Yuzu Aihara.  
sampai satu minggu kemudian Yuzu Aihara memutuskan diam2 mengikuti Mei Aihara.

Di kamar mereka berdebat dan Mei Aihara mendorong Yuzu Aihara ke kasur dan melepas baju Yuzu Aihara.

Karena mendengar keributan kakek langsung masuk ke kamar Mei Aihara dan mengusir Yuzu Aihara

keesokan hari nya Yuzu Aihara langsug ke ruang kepala sekolah, Yuzu Aihara sangat terkejut mengetahui kepala sekolah tergeletak di lantai dan langsung memanggil ambulan.

Mei Aihara sangat berterima kasih kepada Yuzu Aihara atas pertolongan nya kepada kakek.

## Vol.1 Chapter 4: sisterly love?

keesokan harinya Mei Aihara dan Yuzu Aihara bergegas kerumah sakit. Kakek memaafkan Yuzu Aihara dan mengucapkan terima kasih atas pertolongan nya.

Kakek sudah di beri tahu oleh Mei Aihara bahwa Yuzu Aihara adalah cucu nya juga.

Setelah ber bincang2 cukup lama mereka kemudian pulang ke rumah.

Yuzu Aihara membaca manga tentang 2 orang perempuan berhubungan badan (manga yuri) dan membayangkan dirinya melakukan hal tersebut dengan saudara nya yaitu Mei Aihara

Saat di kamar Yuzu Aihara diam diam ingin mencium Mei Aihara dan tiba tiba Mei Aihara bangun.

Saat Mei Aihara beranjak pergi Yuzu Aihara bertanya “kenapa kau tidak memanggil ku dengan nama?”

Mei Aihara menjawab “Aku tidak ingin dekat denganmu yang berasal dari keluarga luar. KARENA AKU TIDAK TERTARIK DENGAN MU!”.

Mei Aihara melanjutkan “saat kau mulai menjadi gangguan” mendekatkan bibir nya ke Yuzu Aihara “cara ini berguna untuk menutup mulutmu. Mengerti?”

![](./Yuzu-Aihara-Dan-Mei-Aihara-Mendekatkan-Bibir.webp)

Yuzu Aihara langsung menangis dan berkata “Mei jahat!. Mei Bodoh!” dan langsung tidur

![](./Yuzu-Aihara-Marah-Kepada-Mei-Aihara.webp)

keesokan hari nya, setelah bersih2 toilet sekolah mei menemukan secarik kertas di tas nya yang bertuliskan “Tolong Datang Ke Ruang Rapat – Aihara Mei”

Yuzu Aihara langsung berlari ke ruang rapat dan Mei Aihara langsung menunjukkan manga yuri kepunyaan Yuzu Aihara

Mei Aihara berkata “Ini. Benda di meja mu. Aku satu2 nya yang melihat benda ini untuk menghindari gunjingan jadi silahkan menyingkir dari”

dengan cepat Yuzu Aihara langsung mencium Mei Aihara.

### Vol.2 Chapter 5: Love me do!

Apa yang kau lakukan di ruangan ketua! Teriak Momokino Himeko (Wakil Ketua OSIS)

Yuzu Aihara langsung membungkam mulut Momokino Himeko dan berkata “Bagaimana jika kita berbicara tentang hal ini besok, dan tidak di sekolah!”

keesokan hari nya di sebuah cafe terjadi dialog antara Yuzu Aihara dan Momokino Himeko

Momokino Himeko bercerita bahwa ia adalah teman masa kecil Mei Aihara, dulunya Mei Aihara adalah pribadi yang ceria dan menjadi pendiam semenjak di tinggal ayah nya

keesokan hari nya Momokino Himeko di ruang OSIS tiba tiba mengelus2 rambut dan mencium Mei Aihara

![](./Momokino-Himeko-Menggigit-Telinga-Mei-Aihara.webp)

dan perebutan Mei Aihara oleh Yuzu Aihara dan Momokino Himeko dimulai!

## Vol.2 Chapter 6: Under Lover

Yuzu Aihara mengajak pergi Mei Aihara. Dan saat di kereta berdesakan lutut Yuzu Aihara tanpa sengaja mengenai “bagian sensitif” Mei Aihara.

Saat Yuzu Aihara sadar lututnya mengenai “bagian sensitif” nya Mei Aihara, Yuzu Aihara malah meneruskan aksinya tersebut dan menjilati telinga dan leher Mei Aihara

Setelah turun dari kereta Yuzu Aihara mentraktir Mei Aihara crepes supaya memaaf kan perbuatan nya.

Dan mereka pun sampai di kuburan ayah nya Yuzu Aihara. Yuzu Aihara Bilang ia akan membantu memperbaiki hubungan Mei Aihara dengan ayah nya

## Vol.2 Chapter 7: No Love

Saat keesokan nya di sekolah Mei Aihara terlalu memaksakan bekerja terlalu keras dan.

GUBRAKK!. Mei Aihara Pingsan Yuzu Aihara menggendong Mei Aihara ke UKS dan Momokino Himeko menggantikan Mei Aihara sebagai pemimpin rapat OSIS

Mei Aihara bercerita ia bekerja sangat keras agar dapat mengambil alih sekolah nya kakek.

Dan ia sangat merindu kan ayah nya supaya dapat membantu mengambil alih sekolah supaya tidak jatuh ke tangan yang salah

ke esokan hari nya Mei Aihara menunjukkan thermometer ke Yuzu Aihara sambil berkata “Yuzu Bangun lah!. Lihat kau tidak akan lagi komentar

Mei Aihara sembuh dari demam hanya dalam satu malam

di ruang keluarga mereka (Yuzu &amp; Mei) mengerjakan tugas tiba2 mendengar pintu terbuka. Tiba 2 masuk seorang laki2 yang langsung memeluk Yuzu Aihara

Yuzu Aihara berteriak “Hentikan dasar mesum!!”

Mei Aihara melihat lelaki tersebut dan berkata “Sensei…..?”

Lelaki tersebut berkata “aku bukan lagi seorang guru. Panggil aku AYAH”

ternyata lelaki tersebut adalah Shou Aihara alias ayahnya Mei Aihara!

Mei Aihara langsung berlari ke dalam dan mengurung diri

Shou Aihara berkata kepada Yuzu Aihara “aku lapar ayo beli makanan”

mereka berbincang bincang . Shou Aihara berkata “Aku bukan ayah yang buruk. Hanya ingin melalui jalan yang berbeda”

saat di kamar Mei Aihara berkata kepada Yuzu Aihara “Ayahku, aku sangat menghormati dia. Yang aku inginkan adalah ayahku kembali ke sekolah”

Mei Aihara melanjutkan “Aku selalu hidup di bawah penderitaan yang ayah buat!!!”

Mei Aihara langsung mencium Yuzu Aihara

## Vol.2 Chapter 8: out of love

Mei Aihara menjilati dan “memainkan area sensitif” Yuzu Aihara

ke esokan hari nya saat sarapan Mei Aihara langsung pergi tanpa sarapan.

“Tunggu mei! Bagaimana dengan sarapan mu!” teriak Yuzu Aihara

Shou Aihara bercerita pada waktu itu Ia meninggalkan Mei Aihara supaya Mei Aihara tidak mengikuti langkah nya yang gagal

saat di kelas Mei Aihara terlihat sangat murung dan lesu

saat di kantin sekolah Yuzu Aihara mendapat telepon bahwa Ayah nya Mei Aihara akan keluar negri lagi

langsung Yuzu Aihara buru2 mencari Mei Aihara. Ternyata Mei Aihara ada di ruang kepala sekolah

Yuzu Aihara Membujuk Mei Aihara supaya mau ke bandara mengantar ayah

saat di bandara Mei Aihara berpesan “jaga diri dan selamat tinggal ayah!”

saat di kamar Mei Aihara membaca surat2 Ayahnya dan tiba2 mencium Mei aihara

## Vol.3 Chapter 9: love or lie!

Saat di game center Yuzu Aihara bertemu Matsuri Misuzawa. Ia adalah teman masa kecil nya Yuzu Aihara . Mereka berbincang dan bercanda

saat pengumuman ujian Mei Aihara takjub mengetahui bahwa Yuzu Aihara mendapat peringkat 97

saat di karaoke Matsuri Misuzawa mengutarakan perasaan nya dengan Yuzu Aihara . “Aku menyukai mu Yuzu-Chan” ucap Matsuri Misuzawa

saat Matsuri Misuzawa mencium Yuzu Aihara tiba tiba telepon Yuzu Aihara berdering yang ternyata dari Mei Aihara

## Vol.3 Chapter 10: love of war

saat Yuzu Aihara mencuci piring tiba tiba Mei Aihara bertanya “kenapa kau pulang telat hari ini?”

Yuzu Aihara menjawab “beberapa tahun yang lalu dia masih terlihat seperti anak kecil. Walaupun dia sudah berambah dewasa tetapi di mata ku dia terlihat seperti anak kecil”

Yuzu Aihara menambahkan “aku sudah menganggap nya seperti adikku sendiri”

tiba tiba Mei Aihara memeluk Yuzu Aihara dan berkata “bagi saudara ini merupakan hal yang normal kan?”

saat di mall Matsuri tiba tiba berlari mengetahui bahwa ada Mei Aihara

Matsuri merencanakan untuk menjebak Mei Aihara

Matsuri menjebak Mei Aihara dengan diam2 memfoto Mei dan dirinya berciuman

## Vol.3 Chapter 11: love is blind

Mei Aihara Dan Yuzu Aihara sedang duduk di halte menunggu bis untuk pulang

Yuzu Aihara bercerita tentang pertemuan Pertama kali nya dengan Matsuri terjadi saat ia pindah rumah

tiba tiba Yuzu aihara merasa kedinginan dan mengajak bergandengan tangan dan tiba tiba turun salju

Mei Aihara melihat papan informasi dan berkata “pantas saja tidak ada yang datang, bus tidak akan datang di hari menjelang liburan”

mereka berdua pun pulang dengan berjalan kaki berdua

saat di sekolah matsuri tiba tiba datang ke sekolah menemui Mei Aihara

Matsuri menunjukkan foto saat ia dan Mei Aihara berciuman dan mengancam akan menyebar luaskan jika tidak menuruti kemauan nya

Matsuri memaksa Mei Aihara berkencan dengan pria yang dikenalnya lewat internet

dengan sangat sedih Mei Aihara terpaksa menuruti Matsuri

di rumah Yuzu Aihara sibuk membuat kue natal untuk Mei Aihara

## Vol.3 Chapter 12: love is

saat dirumah dengan raut wajah sedih Mei Aihara berkata “Besok aku tidak bisa merayakan natal”

saat pulang sekolah Matsuri tiba 2 memeluk Yuzu Aihara dan mengajak nya jalan jalan

saat di cafe Yuzu Aihara Dan Matsuri berdebat dan akhirnya rencana nya Matsuri pun terungkap

Yuzu Aihara langsung berlari mencari Mei Aihara. Dan langsung memeluk Mei Aihara

mereka bertiga naik kereta. Saat di kereta Yuzu Aihara tertidur dan Mei Aihara berbincang dengan Matsuri

Matsuri pun akhirnya menyesali perbuatan nya

saat turun dari kereta Matsuri berbisik kepada Yuzu Aihara “Mei-San sebenarnya dia sangat menyukai mu Yuzu-Chan”

Matsuri berbicara kepada Yuzu dan Mei ayo lakukan 3P ( 3P = 3 Person, s\*x bertiga) IYKWIM 😀

saat Mei Aihara selesai makan kue buatan Yuzu Aihara, Mei Aihara langsung mendorong Yuzu Aihara dan saling memegang “Dada”

dan Mei Aihara berkata kepada Mei Aihara “ayo terlebih dahulu lakukan 2P”

## Vol.4 Chapter 13: winter of love

Mei Aihara berkata kepada Yuzu Aihara “kau telah melakukan banyak hal untuk kakek ku, untuk ku. Aku harus membayar mu dengan apa yang kau inginkan”

(pasti taulah mereka ngapain :v )

keesokan hari nya Matsuri memberikan Chitose Ame (permen stik) kepada Yuzu

ke esokan harinya saat darmawisata Yuzu Aihara bangun kesiangan dan langsung berlari menuju stasiun.

Saat di stasiun Yuzu Aihara bertemu dengan seseorang yang bernama Sara Tachibana

sepenjang perjalanan mereka berbincang2. Ternyata sekolah nya Sara Tachibana juga menginap di hotel yang sama

## Vol.4 Chapter 14: the course of love

Saat di hotel adik nya Sara Tachibana yaitu Nina Tachibana mencari gantungan kunci nya yang hilang

tanpa sengaja mendengar Yuzu Aihara mengungkapkan perasaan nya kepada Mei Aihara

Ternyata Sara Tachibana menyukai Mei Aihara. Yuzu Aihara belum mengetahui hal tersebut

Keesokan harinya mereka ke Kuil Jodoh. Sara Tachibana dan Nina Tachibana juga ke Kuil Jodoh.

Saat Mei Aihara menyendiri tiba2 datang Sara Tachibana yang mengungkapkan perasaan nya

Sara Tachibana : ”Aku menyukai mu!. Ketika kita bertemu di stasiun tokyo. Aku jatuh cinta pada pandangan pertama!

Saat bertemu Yuzu Aihara Mei Aihara berkata “Pagi ini seseorang menyatakan cinta padaku, dan aku memutuskan untuk berpacaran dengan orang itu”

## Vol.4 Chapter 15: love you only

saat sedang di pemandian Aihara Yuzu Bertemu Sara Tachibana. Sara Tachibana bercerita bahwa ia berhasil mengungkapkan perasaan nya

Yuzu Aihara sangat senang dengan hal itu. Yuzu Aihara juga bercerita bahwa orang yang dicintai nya telah berpacaran dengan orang lain

saat mengetahui bahwa Mei Aihara berkencan di Stasiun Kyoto Yuzu Aihara langsung berlari ke Stasiun Kyoto

saat di Stasiun Kyoto Mei Aihara bertemu dengan Nina Tachibana.

Nina Tachibana bercerita bahwa Mei Aihara kencan dengan Sara Tachibana. Ia menyuruh supaya Yuzu Aihara tidak ikut campur

Yuzu Aihara langsung berlari mencari Mei Aihara

## Vol.4 Chapter 16: My love goes on and on

Nina Tachibana langsung menjatuhkan Yuzu Aihara dan berteriak “Aku tidak akan membiarkan mu mencari Mei!”

Nina Tachibana bercerita bahwa kakak nya selalu memikirkan orang lain, kebahagiaan nya nomor dua baginya. Jika kamu bilang bahwa kamu mencintai Mei maka ia akan mengalah.

Tiba2 Sara menelepon Yuzu Aihara. Ia bilang bahwa ia ada di Menara Kyoto

Sara Tachibana bilang kepada Yuzu Aihara bahwa ia sudah mengetahui bahwa Yuzu Aihara mencintai Mei Aihara

Yuzu Aihara mengungkapkan perasaan nya kepada Mei Aihara. Mei Aihara mencium Yuzu Aihara

Mei Aihara tiba tiba menggandeng Yuzu Aihara. Dan berkata “kita akan pulang ke hotel seperti ini!”

**Anime Citrus Season 1 Berakhir Sampai Di Sini.**

Semoga Saja Anime Citrus Mendapat Season Lanjutan

## Vol.5 Chapter 17: to be in love

Saat bangun tidur (adegan nya langsung loncat ke rumah nih) Yuzu Aihara dengan perasaan berbunga bunga ke wastafel bersama Mei Aihara untuk gosok gigi

Yuzu Aihara ber monolog “setelah darma wisata kemarin, orang tua kami menikah sehingga kami menjadi saudara”

(jadi saat mereka darma wisata Shou Aihara dan Ume Aihara menikah. Jadi sepulang nya dari darma wisata mereka resmi jadi saudara tiri)

sepanjang perjalanan Mei Aihara mengomentari gaya berpakaian nya Yuzu Aihara karena akan jadi panutan siswa baru

tiba tiba di tengah perjalanan muncul seseorang yang mengaku penggemar berat nya

saat di sekolah Harumi Taniguchi terlihat sangat gelisah karena kakak nya akan berkunjung ke sekolah

dulu kakak nya merupakan ketua OSIS yang sangat kejam. Dia tidak segan segan untuk menghukum

saat tiba di rumah ibu langsung menyambut dan menggoda Yuzu Aihara karena mempunyai penggemar

ke esokan hari nya Yuzu Aihara tampil dengan rambut hitam

kemudian datang lah siswa baru dengan rambut pirang yang ternyata penggemar nya Yuzu Aihara

saat pulang Yuzu Aihara terkejut karena tiba ada Matsuri. Ternyata maksud kedatangan matsuri adalah untuk menginap

mereka bertiga belajar bersama

## Vol.5 Chapter 18: love birds?!

saat anak baru itu di beri dua pilihan oleh kakak nya Harumi Taniguchi yaitu Mitsuko . Pertama keluar dari sekolah. Kedua potong semua rambut yang berwarna itu.

Tiba 2 muncul Yuzu Aihara yang langsung melepas wig

Mitsuko menantang Yuzu Aihara supaya memenangkan pemilihan OSIS. Jika kalah Yuzu Aihara dan siswa baru yang bernama Nomura Nene dikeluarkan

## Vol.5 Chapter 19: love and friendship

Yuzu Aihara dan penggemarnya yaitu Nomura Nene berbincang bincang dan mendapat kesimpulan bahwa sebenarnya kakak nya Harumi Taniguchi berencana mengeluarkan mereka

Mei Aihara memergoki Nomura Nene mengintip ruang OSIS dan menginterogasi nya

Nomura Nene sangat mengagumi Yuzu Aihara karena dia berbicara tanpa ragu, sangat peduli dan sangat lembut

## Vol.5 Chapter 20: love yourself

Nomura Nene meminta pertolongan Harumi Taniguchi supaya membantu Yuzu Aihara

Harumi Taniguchi dan Yuzu Aihara berbincang2 di taman tentang strategi pemilihan OSIS

Yuzu Aihara berpidato. Dalam pidatonya ia mengaharapkan supaya tidak terpilih menjadi ketua OSIS

## Vol.6 Chapter 21: Dear lover

Yuzu Aihara membuat daftar kencan yang akan di lakukan keesokan harinya bersama Mei Aihara

tetapi buku tersebut diketahui oleh Mei Aihara.

Mei Aihara menyetujui rencana kencan nya, dan ia akan melakusan sesuai daftar tersebut

## Vol.6 Chapter 22: Love notes

Yuzu Aihara dan Mei Aihara pergi kencan, mereka pergi kencan ke bioskop, ke cafe dan ke game center.

Tetapi Mei Aihara terlalu terpaku sama buku karangan Yuzu Aihara, sehingga Yuzu Aihara tidak merasa seperti kencan

Yuzu Aihara melempar buku tersebut karena kencan nya dengan Mei Aihara sangat membosankan

Yuzu Aihara berencana membelikan sepasang cincin untuk dikenakan berdua

saat Mei Aihara tidur Yuzu Aihara diam diam mengukur ukuran jari Mei Aihara

## Vol.6 Chapter 23: the way I love

Yuzu Aihara bekerja paruh waktu di sebuah cafe di kampung halaman nya agar tidak di ketahui sekolah dan Mei Aihara

Yuzu Aihara ketika di tanya oleh Mei Aihara karena pulang larut malam selalu menjawab bahwa ia belajar di rumah Harumin

pada saat gajian ia tiba tiba bertemu Matsuri

Matsuri pesimis dengan apa yang akan diberikan Yuzu Aihara kepada Mei Aihara

setelah cekcok dengan Matsuri, Yuzu Aihara bergegas membeli cincin

setelah membeli sepasang cincin, tiba 2 di tengah perjalanan ia bertemu teman lama nya

ketika mereka sedang asik berbicara tentang percintaan, tiba tiba 2 orang teman nya Yuzu Aihara melihat sepasang perempuan berciuman.

Mereka mengejek habis habisan perilaku tersebut. Yuzu Aihara hanya bisa tertunduk diam

sesampainya di rumah ia menangis di depan Mei Aihara dan memeluk Mei Aihara

## Vol.6 Chapter 24: To not give up on love

Yuzu Aihara di beritahu oleh guru bahwa ia tidak lulus ujian dan harus mengikuti pelajaran tambahan selama libur musim panas

Yuzu Aihara memutuskan untuk ke perpustakaan untuk belajar

saat di perpustakaan ia tiba tiba bertemu Mei Aihara

Mereka saling curhat. Mei Aihara berbiara kepada Yuzu Aihara “Kita adalah saudara tiri yang berpacaran satu sama lain, bagaimana orang lain menerima ini”

Mei Aihara menambahkan “tidak ada jawaban yang tepat, kita hanya melakukan apa yang kita mau”

Yuzu Aihara memeluk Mei Aihara dan berkata “apa tidak apa apa jika aku mencintai mu”

Mei Aihara menjawab “iya”

saat di kamar Yuzu Aihara memberikan cincin yang dibelinya kepada Mei Aihara

saat Yuzu Aihara memakaikan cincin Mei Aihara tiba2 menangis. Yuzu Aihara mengusap air mata nya Mei Aihara

Mei Aihara bilang “sepertinya terlalu memakan banyak waktu. Kamu bisa memakai cincin mu sendiri”

dan Mei Aihara langsung tidur

Yuzu Aihara berteriak “Tunggu Mei! Bangun!. Pasangkan cincin ini di jariku!”

## Vol.7 Chapter 25: Love one another

Harumin menakut nakuti Yuzu Aihara tentang hantu pelajaran tambahan musim panas

tiba tiba Yuzu Aihara menduduki perempuan misterius. Ia sangat mengenal Yuzu Aihara mulai data pribadi sampai ukuran “dada”

perempuan misterius tersebut bercerita kepada Yuzu Aihara bahwa ia memang suka mengamati orang

perempuan misterius tersebut memperkenalkan diri nama nya adalah Siraho Suzuran

## Vol.7 Chapter 26: in fear of love

Siraho Suzuran bilang kepada Yuzu Aihara bahwa Mei Aihara “bukan manusia”.

Ia perpendapat demikian karena Mei Aihara tidak mempunyai celah sedikitpun untuk mendapatkan informasi tentang nya

saat di rumah Yuzu Aihara menceritakan tentang Siraho Suzuran.

Tiba tiba Ibu bilang bahwa minggu ini ada festival. Ibu menyarankan supaya mereka berdua ikut

tiba tiba Mei Aihara menyuapi Yuzu Aihara

saat Yuzu Aihara mandi tiba tiba Mei Aihara ikut bergabung

Yuzu Aihara mencoba menganalisa pikiran nya Mei aihara

Yuzu Aihara pun menyerah karena tidak bisa menganalisa pikiran nya Mei Aihara

saat di kamar Yuzu Aihara bertanya kepada Mei Aihara mengapa ia menyuapi dirinya dan mandi bersama

Mei Aihara menjawab “menurutmu kenapa?”

Yuzu Aihara membalas “aku tidak tahu karena itu aku bertanya”

Mei Aihara “aku juga tidak tahu kenapa aku merasa seperti ini, oleh karena itu aku akan meyakinkannya”

Mei Aihara tiba tiba memeluk Yuzu Aihara

keesokan hari nya saat berangkat mengikuti kelas tambahan Yuzu Aihara bertemu Siraho Suzuran. Seperti biasa ia selalu menanyakan tentang Mei Aihara

## Vol.7 Chapter 27: the one you love

Siraho Suzuran dan yang lain nya mengikuti festival kembang api

mereka membeli berbagai jajanan

Mei Aihara menginginkan sebuah boneka Mei Aihara gagal mendapatkan nya

Yuzu Aihara berhasil mendapatkan nya hanya dalam sekali percobaan

Siraho Suzuran berkata kepada Yuzu Aihara bahwa Mei Aihara sangat khawatir terhadap kalung nya sepanjang waktu

tiba tiba Yuzu Aihara berlari menemui Mei Aihara dan mengajak nya ke tempat sepi untuk melihat kembang api dan berciuman

## Vol.7 Chapter 28: love.exe

Yuzu Aihara tiba 2 berlari meniggalkan Mei Aihara

tiba tiba Mei Aihara bertemu dan mengobrol bersama Siraho Suzuran

Siraho Suzuran menelepon Yuzu Aihara bahwa ia dan Mei Aihara sudah pulang bersama

saat di rumah Yuzu Aihara Membahas tentang perkataan Mei Aihara saat di festifal “aku belum puas”

mereka pun berciuman sampai puas

## Vol.8 Chapter 29: Summer of love

Mei Aihara dan Yuzu Aihara berencara keluar bersama Harumin

Mei Aihara dan Yuzu Aihara saling mempermasalahkan bekas ciuman di leher mereka (itu ciuman / gigit2 an sih kok sampai membekas gitu 😀 )

## Vol.8 Chapter 30: Secret Love

## Vol.8 Chapter 31: Live to Love

## Vol.8 Chapter 32: Lovey Dovey

**Skip. udh g sabar :v . update menyusul**

## Vol.10 Chapter 41 Love Forever

## Vol.10 Chapter 41.5 Epilogue

![](./mei-yuzu-menikah.webp)

Ending nya mereka berdua menikah gan.

---
title: "Alasan Kenapa Wanita Jepang tidak Menyukai Pria Luar Negeri"
date: "2020-07-11T04:50:00+07:00"
---

![Alasan Kenapa Wanita Jepang tidak Menyukai Pria Luar Negeri](./charai.webp)

Di Jepang, ada mitos yang beredar, bahwa orang asing akan dengan mudah berkencan dengan pria atau wanita Jepang. Namun, faktanya, ada banyak alasan yang membuat para wanita Jepang tidak menyukai pria dari negara lain. Seorang Ilustrator Jepang, Mizuka Inaba, membuat ilustrasi kenapa wanita Jepang tidak menyukai pria luar negeri. Yuk, simak alasannya.

## Kendala Bahasa

Tentu saja, kendala bahasa adalah hal paling umum yang ditakuti oleh wanita Jepang. Bukan hanya dalam percakapan, tetapi mereka juga takut kalau pria tersebut tidak bisa memahami berbagai masalah seperti visa, pajak, asuransi kesehatan, pendidikan, bahkan gaya potongan rambut, dan cara memesan makanan di Jepang.

## Stigma Publik

Sebagian besar budaya di Jepang adalah homogen, masih mempertahankan budaya asli mereka, dan 2% dari keseluruhannya merupakan adaptasi budaya luar. Karena perbedaan budaya yang signifikan ini, sangat sedikit dari pasangan antar negara yang berhasil sampai ke pelaminan, dan sebagian besar yang berhasil pun adalah pasangan pria Jepang dengan wanita asing. Menurut survei, dari seluruh wanita Jepang yang menikah, hanya 1,3% yang menikah dengan pria asing.

## Persetujuan Orangtua

Sama halnya dengan stigma publik, hal ini juga berlaku pada figur paling penting bagi seorang wanita Jepang, ayah dan ibunya. Bukan berarti rasis, tapi terkadang orangtua di Jepang masih mempraktekkan beberapa budaya kuno untuk hubungan anaknya. Wanita Jepang biasanya masih tinggal dengan orangtuanya sampai dengan usia 20-an, itulah sebabnya kenapa mereka lebih memprioritaskan orangtua dari pada pacar mereka.

## Pekerjaan Berpenghasilan Rendah

Meskipun tidak semua wanita Jepang menilai seorang pria asing dari jumlah penghasilan mereka, tapi beberapa memang melakukan hal itu. Beberapa wanita Jepang menganggap penghasilan rata-rata dari pria asing yang bekerja di Jepang tidak cukup untuk membiayai keluarga dalam Jangka panjang, dan mungkin tidak cukup untuk biaya kencan. Tergantung wanita itu sendiri, yang lebih muda biasanya tidak begitu peduli dengan penghasilan.

## Charai

Orang asing, terutama yang berasal dari negara Barat, dicap sebagai “charai” atau “playboy”. Hal inilah yang membuat munculnya mitos orang asing bisa dengan mudah berkencan dengan wanita Jepang. Meskipun tidak semuanya seperti itu, tapi begitulah pandangan orang-orang Jepang terhadap pria asing.

## Hanya "Transit"

Realita yang ada saat ini, kebanyakan orang asing hanya tinggal sementara di Jepang. Baik hanya seminggu atau beberapa tahun, akan ada saatnya dimana mereka harus pulang ke negara mereka. Hal ini membuat mustahil untuk memberikan wanita Jepang perasaan aman yang sama dengan jika mereka berkencan dengan pria Jepang. Semua wanita Jepang tau hal ini, bahkan mesipun mereka belum pernah berpacaran dengan orang asing sebelumnya.

## Kuki Yomenai

“Kuki yomenai” berarti pria asing tidak bisa membaca situasi, atau secara sosial budaya sangat berbeda. Seperti berbicara di telefon dengan suara keras di dalam kereta, atau bermesraan yang berlebihan di tempat umum. Hal ini dianggap sebagai maner yang buruk, tidak menghargai budaya, dan keterampilan sosial yang buruk. Meskipun, sekali lagi, tidak semua pria asing seperti ini, dan juga tergantung dari negara mana ia berasal.

\[[Japanese Station](https://japanesestation.com/lifestyle/life-relationship/alasan-kenapa-wanita-jepang-tidak-menyukai-pria-luar-negeri?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

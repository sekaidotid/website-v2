---
title: "Japan Foundation Jakarta Menutup Sementara Koryu Space Karena COVID-19"
date: "2020-03-14T09:15:00+07:00"
---

![Japan Foundation Jakarta Menutup Sementara Koryu Space Karena COVID-19](./sekaidotid-jf-jakarta.webp)

semakin banyak nya tempat yang di tutup sementara akibat COVID-19, salah satunya adalah Koryu Space Japan Foundation Jakarta

seperti pengumuman yang di tilis di Twitter @@JF_Jakarta berikut

> PENGUMUMAN
> KORYU SPACE TUTUP UNTUK UMUM
> 13 - 31 MARET 2020
>
> Menindaklanjuti situasi terkini terkait COVID-19 di Indonesia, JF Jakarta >memutuskan menutup sementara Koryu Space untuk pengunjung umum mulai tanggal 13 >hingga 31 Maret 2020.
>
> Terima kasih untuk pengertiannya.
>
> Twitter @JF_Jakarta 17:05 12 Maret 2020

---
title: "Log Horizon Season 3 Di Tunda Ke Winter 2021 Akibat COVID-19"
date: "2020-06-15T15:39:00+07:00"
---

![Log Horizon Season 3 Di Tunda Ke Winter 2021 Akibat COVID-19](./sekaidotid-log-horizon-3.webp)

dari website NHK

> 今年１０月にＥテレで放送を予定しておりました
> 新作アニメ「ログ・ホライズン　円卓崩壊」（全１２話）につきまして、
> 新型コロナウイルスの感染拡大により、
> 当初の制作スケジュールに大きな影響が生じていることから、
> Ｅテレでの放送を【来年１月（２１年１月）スタート】に変更・延期することになりました。
>
> 詳しい放送日時については、あらためてご案内いたしますが
> ご理解・ご了承いただけますようお願い申し上げます。
>
> また、最新キービジュアルがあわせて公開されております。
> 今後のお知らせ、そして、放送開始までしばらくお待ちいただけると幸いです。
>
> Kami berencana untuk menyiarkan di E-televisi pada bulan Oktober tahun ini
> Mengenai animasi baru "Log Horizon Roundtable Collapse" (12 episode)
> Karena penyebaran infeksi coronavirus baru,
> Karena jadwal produksi awal memiliki dampak besar,
> Siaran di E-televisi akan diubah dan ditunda ke [Mulai Januari mendatang (21 Januari)].
>
> Kami akan memberi tahu Anda tentang tanggal dan waktu siaran terperinci, tetapi
> Kami menghargai pengertian dan pengertian Anda.
>
> Selain itu, visual kunci terbaru juga dirilis.
> Kami harap Anda bisa menunggu sebentar di berita dan siaran mendatang.

\[[NHK](https://www6.nhk.or.jp/anime/topics/detail.html?i=9616?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

---
title: "Rangkuman Manga Itai no wa Iya nano de Bougyoryoku ni Kyokufuri Shitai to Omoimasu"
date: "2020-01-10T06:05:41+07:00"
---

![](./Itai-no-wa-Iya-nano-de-Bougyoryoku-ni-Kyokufuri-Shitai-to-Omoimasu.webp)

Honjou Kaede di ajak oleh teman nya Risa untuk bermain sebuah game VRMMORPG yang bernama “New World Online” dengan nama pemain “Maple (メイプル)”

sekedar info nama pemain Risa adalah Sally ( サリー )

karena tidak mau menerima dampak serangan maka dia memilih tameng dan pedang kecil sebagai perlengkapan pertama, dan menghabiskan status poin nya utk meningkatkan VIT

![](./vlcsnap-2020-01-09-20h39m55s654.webp)

sebagai permulaan dia farming monster di dekat kota dulu, untuk meningkatkan pertahanan dia membiarkan dirinya <s>dig4ngb4ng</s> diserang oleh para monster

![](./vlcsnap-2020-01-09-20h41m57s979.webp)

setelah mendapatkan informasi mengenai dungeon. maka Maple pergi ke dungeon sendirian

![](./vlcsnap-2020-01-09-20h43m23s884.webp)

saat di dungeon dia bertarung melawan naga beracun. lagi2 dia “menikmati” serangan monster ( kok kayak Darkness dari anime konosuba ya )

![](./vlcsnap-2020-01-09-20h44m25s909.webp)

untuk mengalahkan naga tersebut, dia menggigit naga tersebut dan memakan nya. setelah memakan naga tersebut dia mendapatkan kekuatan Hydra Eater yaitu kebal racun &amp; kemampuan serangan racun

dia juga mendapat perlengkapan yang OP BANGET! saat perlengkapan nya rusak akan otomatis kembali seperti semula dan bertambah kuat.

setelah mengalahkan naga, dia berencana mengikuti turnamen battle royal. dia kembali melakukan farming untuk meningkatkan kemampuan nya

saat mengikuti battle royal, tentu saja pertahanan nya itu mutlak walau diserang oleh banyak orang sekalipun

hal tsb mengantarkan nya ke posisi 3

setelah perlombaan tentu dia menjadi bahan perbincangan banyak orang

saat sepulang sekolah dia di beritahu bahwa ada update besar dengan penambahan lantai 2 di game tsb

kemudian dia mendatangi pengrajin senjata dan menginginkan perlengkapan serba putih

kemudian dia sebuah danau untuk memancing ikan putih. kerena AGI &amp; DEX nya 0 maka ia hanya mendapat 1 ikan dalam waktu 20 menit

sepulang sekolah Maple &amp; Sally memutuskan utk mabar. Maple mengajak Sally memancing ikan putih

setelah 2 jam memancing Sally mendapat 24 ikan, Maple hanya mendapat 6 ikan

kemudian Sally memutuskan utk menyelam dan menyerang ikan

sally saat menyelam menemukan gua di dalam danau, dia memutuskan utk nenelusuri goa tersebut dan menemukan ada ruangan boss

dia berhasil mengalahkan boss dan berhasil mendapatkan perlengkapan

kemudian maple mendatangi pengrajin utk membuat tameng, tetapi dia kekurangan material

dia diminta utk menjadi pengawal pengrajin utk menambang material, tentu saja dapat mengalahkan berbagai monster dengan sangat mudah

![](./bofuri-maple-tate.webp)

tameng barunya (Snow White) meningkatkan VIT +40, dibanding yang lama (Dark Knight Replica) VIT +20

setelah mendapatkan tameng baru Maple &amp; Sally mengikuti sebuah event yang dimana mereka di teleport di sebuah padang rumput yang sangat luas

kemudian mereka menemukan tempat raja goblin, sudah bisa ditebak mereka bisa mengalahkan nya dengan mudah

kemudian mereka memasuki hutan yang sangat lebat

tiba-tiba muncul api hijau di sekeliling mereka, Sally yang merasa sangat ketakutan langsung menggendong Maple

mereka langsung memasuki bangunan di dalam hutan untuk berlindung

kemudian mereka mendengar suara aneh, Sally yang ketakutan langsung melompat ke Maple

ternayata ada <s>sebuah</s> seorang NPC yang terluka, Sally memutuskan untuk mengoobatinya

Saat pagi mereka bergegas keluar dari hutan

kemudian mereka pergi ke gunung

di puncak gunung mereka melawan boss burung raksasa

setelah mereka melalui pertarungan yang sangat sengit (HP nya Maple saja sampai hampir habis)

akhirnya mereka memenangkan pertarungan tersebut

tetapi hal tersebut membuat panik Admin game tersebut, karena mereka merancang monster tersebut (Silver Wing) tidak dapat dikalahkan oleh pemain

**Harap Bersabar Menunggu Update Ya 🙂**

---
title: "Go-Toubun no Hanayome Rayakan Chapter Terakhir Manga dengan Acara Pernikahan"
date: "2020-02-20T15:00:00+07:00"
---

![](./go-toubun-nikah.webp)

Vendor karya seni digital Anique berkolaborasi dengan manga karya Negi Haruba, “Go-Toubun no Hanayome” untuk membuat upacara pernikahan untuk masing-masing saudari selama lima hari di bulan April. Pengumuman rencana ini datang bersamaan dengan publikasi bab terakhir manga pada hari Rabu.

Ilustrasi besar dari kelima bersaudari dalam gaun pengantin akan ditampilkan di Ivy Hall Glory Chapel di Shibuya dari 10 April hingga 14 April. Hanya “mitra” yang diizinkan untuk mendaftar ke acara tersebut dengan dasar “datang pertama dilayani pertama”. Kalian dapat mengajukan permohonan untuk menjadi “mitra” dengan membeli seni digital heroine pilihan seharga ¥ 1.500. Kalian akan menerima sertifikat digital dengan nama kalian yang ditulis bersama Negi Haruba, editor Shintaro Kawakubo, dan nama lain yang terlibat dalam pembuatan seri ini.

Mendaftar sebagai “mitra” juga akan memberikan akses untuk membeli karya seni dan merchandise eksklusif. Rincian lebih lanjut, termasuk cara mendaftar, ada di situs Anique.

Majalah Weekly Shonen Kodansha juga telah memasang produk baru di toko online Rakuten-nya untuk memperingati bab terakhir. Barang-barang termasuk kotak-kotak yang bisa menampung empat belas volume manga, dan clear file manga.

Go-toubun no Hanayome telah mendapat adaptasi anime untuk musim pertamanya yang tayang pada tanggal 10 Januari 2019 dan tayang selama 12 episode. Musim pertamanya diproduksi oleh studio Tezuka Productions dan disutradarai oleh Satoshi Kuwabara (Dagashi Kashi 2, Yu-Gi-Oh! Zexal). Musim keduanya akan tayang Oktober nanti dan dikerjakan oleh studio Bibury.

Seri ini menceritakan tentang Uesugi Fuutarou, siswa jenius yang memberikan impresi buruk pada Nakano Itsuki saat keduanya pertama kali bertemu. Fuutarou yang keluarganya mengalami kesulitan finansial mendapat tawaran kerja untuk mengajar putrinya dari seseorang yang ternyata adalah ayah Itsuki. Hanya saja saat Uesugi berusaha untuk memperbaiki hubungannya dengan Itsuki, ternyata alasan dia pekerjaan ini memiliki bayaran tinggi adalah dia harus mengajar dan mendekati kelima bersaudari Nakano yang masing-masing memiliki masalah sendiri.

Haruba memulai serial manga ini di majalah Weekly Shounen Magazine pada 9 Agustus 2017. Kodansha sebelumnya menerbitkan volume ke-13 pada 13 Januari 2020. Kodansha juga merilis sebuah buku karakter dari karakter Nakano Ichika pada 15 November 2019. Edisi buku karakter berikutnya adalah Nakano Nino yang diterbitkan pada 17 Desember, lalu buku karakter Nakano Miku diterbitkan pada 17 Januari 2020. Manga ini akan tamat pada terbitan volume 14.

Manga ini juga diterbitkan di Indonesia oleh Penerbit Elex Media dengan judul The Quintessential Quintuplets. \[[Jurnal Otaku](http://jurnalotaku.com/2020/02/20/go-toubun-no-hanayome-rayakan-chapter-terakhir-manga-dengan-acara-pernikahan/?utm_source=SEKAI.ID&utm_medium=SEKAI.ID_Source_Link&utm_campaign=SEKAI.ID_Source_Link)\]

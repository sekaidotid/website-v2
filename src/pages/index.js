import { Link, graphql } from "gatsby"

import AdSense from "react-adsense"
import Bio from "../components/bio"
import Layout from "../components/layout"
import React from "react"
import SEO from "../components/seo"

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title
  const posts = data.allMarkdownRemark.edges

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title="Beranda" />
      <div className="card">
        <AdSense.Google
          client="ca-pub-6599086733747900"
          slot="7220962165"
          style={{ display: "block" }}
          format="auto"
          responsive="true"
        />
      </div>
      <Bio />
      {posts.map(({ node }) => {
        const title = node.frontmatter.title || node.fields.slug
        return (
          <article className="card" key={node.fields.slug}>
            <header className="text-2xl text-sekai">
              <Link className="" to={node.fields.slug}>
                {title}
              </Link>
            </header>
            <section>
              <p
                className="text-2xl"
                dangerouslySetInnerHTML={{
                  __html: node.frontmatter.description || node.excerpt,
                }}
              />
            </section>
          </article>
        )
      })}
      <div className="card">
        <AdSense.Google
          client="ca-pub-6599086733747900"
          slot="7220962165"
          style={{ display: "block" }}
          format="auto"
          responsive="true"
        />
      </div>
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
  }
`

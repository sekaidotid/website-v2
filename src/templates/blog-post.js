import { Link, graphql } from "gatsby"

import AdSense from "react-adsense"
import Bio from "../components/bio"
import Layout from "../components/layout"
import React from "react"
import SEO from "../components/seo"

const BlogPostTemplate = ({ data, pageContext, location }) => {
  const post = data.markdownRemark
  const siteTitle = data.site.siteMetadata.title
  const { previous, next } = pageContext

  return (
    <Layout location={location} title={siteTitle}>
      <SEO
        title={post.frontmatter.title}
        description={post.frontmatter.description || post.excerpt}
      />
      <article>
        <header>
          <h1 className="mt-8 mb-0 font-bold text-sekai">
            {post.frontmatter.title}
          </h1>
        </header>
        <Bio />
        <div className="card">
          <AdSense.Google
            client="ca-pub-6599086733747900"
            slot="7220962165"
            style={{ display: "block" }}
            format="auto"
            responsive="true"
          />
        </div>

        <section
          className="blog-post"
          dangerouslySetInnerHTML={{ __html: post.html }}
        />
        <div className="card">
          <AdSense.Google
            client="ca-pub-6599086733747900"
            slot="7220962165"
            style={{ display: "block" }}
            format="auto"
            responsive="true"
          />
        </div>
        <footer>
          <Bio />
        </footer>
      </article>
      <nav>
        <ul>
          <li className="card">
            {previous && (
              <Link to={previous.fields.slug} rel="prev">
                ← {previous.frontmatter.title}
              </Link>
            )}
          </li>
          <li className="card">
            {next && (
              <Link to={next.fields.slug} rel="next">
                {next.frontmatter.title} →
              </Link>
            )}
          </li>
        </ul>
      </nav>
    </Layout>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        description
      }
    }
  }
`

import { Link } from "gatsby"
import Logo from "../../content/assets/sekaidotid-logo-white-web.png"
import React from "react"

const Layout = ({ location, title, children }) => {
  let header

  header = (
    <div className="flex flex-wrap items-center justify-between p-6 border shadow-lg bg-sekai">
      <div className="flex items-center flex-shrink-0 mr-6 shadow-none logo">
        <Link to={`/`}>
          <img src={Logo} alt="SEKAI.ID Logo" />
        </Link>
      </div>
      <div className="flex-grow block w-full shadow-none lg:flex lg:items-center lg:w-auto">
        <div className="text-base lg:flex-grow">
          <a
            href="https://twitter.com/sekaidotid"
            className="block mt-4 mr-4 text-white lg:inline-block lg:mt-0 hover:text-white"
          >
            Twitter
          </a>
          <a
            href="https://facebook.com/sekaidotid"
            className="block mt-4 mr-4 text-white lg:inline-block lg:mt-0 hover:text-white"
          >
            Facebook
          </a>
          <a
            href="https://instagram.com/sekaidotid"
            className="block mt-4 mr-4 text-white lg:inline-block lg:mt-0 hover:text-white"
          >
            Instagram
          </a>
          <a
            href="https://t.me/sekaidotid_grup"
            className="block mt-4 mr-4 text-white lg:inline-block lg:mt-0 hover:text-white"
          >
            Telegram
          </a>
          <a
            href="https://gitlab.com/sekaidotid"
            className="block mt-4 mr-4 text-white lg:inline-block lg:mt-0 hover:text-white"
          >
            GitLab
          </a>
        </div>
      </div>
    </div>
  )

  return (
    <div className="bg-sekai2">
      <div className="max-w-4xl px-5 py-5 mx-auto bg-white card">
        <header>{header}</header>
        <main>{children}</main>
        <footer className="text-2xl card">
          <div className="font-bold">
            <a
              href="https://saweria.co/sekaidotid"
              target="_blank"
              rel="noreferrer"
            >
              Dukung SEKAI.ID Via saweria.co
            </a>
          </div>
          &#127279; 2018 - {new Date().getFullYear()} SEKAI.ID,{" "}
          <a href="https://creativecommons.org/licenses/by-sa/4.0/">
            CC BY-SA 4.0
          </a>
          . Built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a> <br />
          <a href="https://s.sekai.id/git-web">
            &#12304; Source Code on GitLab &#12305;
          </a>
        </footer>
        <script
          async
          src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
        ></script>
      </div>
    </div>
  )
}

export default Layout
